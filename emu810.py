# emu810.py 2015/8/22
# coding:utf-8
#
import sys
sys.path.append("C:/python27/Lib")
import collections
import EMU81x

input = collections.deque()

def SerialGetc_Callback(ch):
    global input
    c = input.popleft()
    if c == '\n': return '\r'
    return c

def SerialReadable_Callback(ch):
    global input
    return 1 if len(input) > 0 else 0

if __name__=="__main__":
    print "LPC810 emulator pre-alpha version"
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('infile', nargs=1)
    parser.add_argument('--limit', type=int, default=1000000)
    parser.add_argument('--flash_size', type=int, default=4*1024)
    parser.add_argument('--ram_size', type=int, default=1*1024)
    parser.add_argument('--rom', default="LPC812.ROM")
    parser.add_argument('--input')
    args = parser.parse_args()
    filename = args.infile[0]
    if args.input:
        input = collections.deque(open(args.input).read())

    mcu = EMU81x.EMU81x(args.flash_size, args.ram_size)
    mcu.SerialPutc_Callback = lambda ch,c: sys.stdout.write(c)
    mcu.SerialGetc_Callback = SerialGetc_Callback
    mcu.SerialReadable_Callback = SerialReadable_Callback
    mcu.DigitalWrite_Callback = lambda port,pin,value: sys.stdout.write("PIO%d_%d << %d\n" % (port, pin, value))
    mcu.I2CWrite_Callback = lambda addr,data: sys.stdout.write("I2C(0x%02x) << %s\n" % (addr, data))
    mcu.I2CRead_Callback = lambda addr: sys.stdout.write("I2C(0x%02x) >> ...\n" % addr)
    mcu.load_flash(filename)
    if args.rom:
        mcu.load_rom(args.rom)
    mcu.reset()
    for _ in xrange(args.limit):
        mcu.run(20)
        mcu.clock_in(1000000)

