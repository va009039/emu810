# emu1114.py 2015/8/22
# coding:utf-8
#
import sys
sys.path.append("C:/python27/Lib")
import collections
import EMU111x

input = collections.deque()

def SerialGetc_Callback(ch):
    global input
    c = input.popleft()
    if c == '\n': return '\r'
    return c

def SerialReadable_Callback(ch):
    global input
    return 1 if len(input) > 0 else 0

if __name__=="__main__":
    print "LPC1114 emulator pre-alpha version"
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('infile', nargs=1)
    parser.add_argument('--limit', type=int, default=1000000)
    parser.add_argument('--flash_size', type=int, default=32*1024)
    parser.add_argument('--ram_size', type=int, default=4*1024)
    parser.add_argument('--input')
    args = parser.parse_args()
    filename = args.infile[0]
    if args.input:
        input = collections.deque(open(args.input).read())

    mcu = EMU111x.EMU111x(args.flash_size, args.ram_size)
    mcu.SerialPutc_Callback = lambda ch,c: sys.stdout.write(c)
    mcu.SerialGetc_Callback = SerialGetc_Callback
    mcu.SerialReadable_Callback = SerialReadable_Callback
    mcu.DigitalWrite_Callback = lambda port,pin,value: sys.stdout.write("PIO%d_%d << %d\n" %(port,pin,value))
    mcu.load_flash(filename)
    mcu.reset()
    for _ in xrange(args.limit):
        mcu.run(256)
        mcu.clock_in(20000)

