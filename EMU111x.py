# EMU111x.py 2015/8/19
# coding:utf-8
#
import sys
import logging
import BaseV6M

EMU111x_FLASH_BASE = 0x00000000
EMU111x_RAM_BASE =   0x10000000
EMU111x_ROM_BASE =   0x1fff0000
EMU111x_APB_BASE =   0x40000000
EMU111x_AHB_BASE =   0x50000000
EMU111x_ROM_SIZE = 16*1024

CMSIS_SCB_VTOR = 0xe000ed00+0x08

class EMU111x_SYSCON:
    def poke32(self, a, d):
        logging.info("P: LPC_SYSCON %08x << %08x", a, d)

    def peek32(self, a):
        d = 0x00000000
        if a == 0x4004800c or a == 0x40048044 or a == 0x40048074:
            d = 0x00000001
        logging.info("P: LPC_SYSCON %08x >> %08x", a, d)
        return d

class EMU111x_IOCON:
    def __init__(self):
        self.table = {
            0x40044030: ["PIO0_4",    0xd0],
            0x40044034: ["PIO0_5",    0xd0],
            0x4004404c: ["PIO0_6",    0xd0],
            0x40044050: ["PIO0_7",    0xd0],
            0x40044060: ["PIO0_8",    0xd0],
            0x40044064: ["PIO0_9",    0xd0],
            0x40044074: ["R_PIO0_11", 0xd0],
            0x400440a0: ["PIO1_5",    0xd0],
            0x400440a4: ["PIO1_6",    0xd0],
            0x400440a8: ["PIO1_7",    0xd0],
            0x400440b0: ["SCK_LOC",   0x00],
        }

    def poke32(self, a, d):
        if a in self.table:
            self.table[a][1] = d
            logging.info("P: LPC_IOCON->%s << %08x", self.table[a][0], d) 
        else:
            logging.warn("P: LPC_IOCON %08x << %08x", a, d)

    def peek32(self, a):
        d = 0x00000000
        if a in self.table:
            d = self.table[a][1]
            logging.info("P: LPC_IOCON->%s >> %08x", self.table[a][0], d) 
        else:
            logging.warn("P: LPC_IOCON %08x >> %08x", a, d)
        return d

class EMU111x_GPIO:
    def __init__(self, mcu, port):
        self.mcu = mcu
        self.port = port
        self.data = 0x00
        self.dir = 0x00

    def poke32(self, a, d):
        if (a&0xffff) == 0x3ffc:
            self.data = d
            for pin in xrange(32):
                mask = 1<<pin
                if self.dir & mask: # output
                    self.mcu.DigitalWrite_Callback(self.port, pin, 1 if (d & mask) else 0)
            logging.info("P: LPC_GPIO%d->DATA %08x << %08x", self.port, a, d)
        elif (a&0xffff) == 0x8000:
            self.dir = d
            logging.info("P: LPC_GPIO%d->DIR << %08x", self.port, d)
        else:
            logging.warn("P: LPC_GPIO%d %08x << %08x", self.port, a, d)

    def peek32(self, a):
        d = 0x00
        if (a&0xffff) < 0x3ffc:
            mask = (a>>2)&0xfff
            for pin in xrange(24):
                if mask & (1<<pin):
                    if mcu.DigitalRead_Callback(self.port, pin):
                        d |= (1<<pin)
            logging.warn("P: LPC_GPIO%d->DATA %08x >> %08x", self.port, a, d)
        elif (a&0xffff) == 0x3ffc:
            d = self.data
            logging.info("P: LPC_GPIO%d->DATA %08x >> %08x", self.port, a, d)
        elif (a&0xffff) == 0x8000:
            d = self.dir
            logging.info("P: LPC_GPIO%d->DIR >> %08x", self.port, d)
        else:
            logging.warn("P: LPC_GPIO%d %08x >> %08x", self.port, a, d)
        return d

class EMU111x_TMR32B:
    def __init__(self, ch):
        assert ch == 0 or ch == 1
        self.ch = ch
        self.tc = 0

    def clock_in(self, n = 1):
        assert 0x7fffffff > n >= 1
        self.tc = (self.tc + n) & 0xffffffff

    def poke32(self, a, d):
        if (a&0xff) == 0x04:
            logging.info("P: LPC_TMR32B%d->TCR << 0x%08x", self.ch, d)
        elif (a&0xff) == 0x0c:
            logging.info("P: LPC_TMR32B%d->PR << 0x%08x", self.ch, d)
        else:
            logging.warn("P: LPC_TMR32B%d %08x << %08x", self.ch, a, d)

    def peek32(self, a):
        d = 0x00000000
        if (a&0xff) == 0x08:
            logging.info("P: LPC_TMR32B%d->TC >> %d", self.ch, self.tc)
            d = self.tc
        else:
            logging.warn("P: LPC_TMR32B%d %08x >> %08x", self.ch, a, d)
        return d

class EMU111x_UART:
    def __init__(self, mcu):
        self.mcu = mcu
        self.DLL = 0x01
        self.DLM = 0x00
        self.LCR = 0x00
        self.FDR = 0x00

    def poke32(self, a, d):
        if (a&0xff) == 0x00:
            if self.LCR & 0x80: # DLAB=1
                self.DLL = d
                logging.info("P: LPC_UART->DLL << %08x", d)
            else: # DLAB=0
                c = chr(d&0xff)
                self.mcu.SerialPutc_Callback(0, c)
                logging.info("P: LPC_UART->THR << %08x", d)
        elif (a&0xff) == 0x04:
            if self.LCR & 0x80: # DLAB=1
                self.DLM = d
                logging.info("P: LPC_UART->DLM << %08x", d)
            else: # DLAB=0
                logging.info("P: LPC_UART->IER << %08x", d)
        elif (a&0xff) == 0x08:
            logging.info("P: LPC_UART->FCR << %08x", d)
        elif (a&0xff) == 0x0c:
            self.LCR = d
            logging.info("P: LPC_UART->LCR << %08x", d)
        elif (a&0xff) == 0x28:
            self.FDR = d
            logging.info("P: LPC_UART->FDR << %08x", d)
        else:
            logging.warn("P: LPC_UART %08x << %08x", a, d)

    def peek32(self, a):
        d = 0x00
        if (a&0xff) == 0x00:
            if self.LCR & 0x80: # DLAB=1
                d = self.DLL
                logging.info("P: LPC_UART->DLL >> %08x", d)
            else: # DLAB=0
                c = self.mcu.SerialGetc_Callback(0)
                d = ord(c)
                logging.info("P: LPC_UART->RBR >> %08x", d);
        elif (a&0xff) == 0x0c:
            d = self.LCR
            logging.info("P: LPC_UART->LCR << %08x", d)
        elif (a&0xff) == 0x14:
            d = 0x20
            if self.mcu.SerialReadable_Callback(0):
                d |= 0x01
            logging.info("P: LPC_UART->LSR >> %08x", d)
        else:
            logging.warn("P: LPC_UART %08x >> %08x", a, d)
        return d

class EMU111x_I2C:
    def __init__(self, mcu):
        self.mcu = mcu
        self.con = 0
        self.stat = 0xf8
        self.i2c_addr = 0x00
        self.i2c_data = []
        self.i2c_pos = 0

    def poke32(self, a, d):
        if (a&0xff) == 0x00: # CONSET
            if d == 0x40:
                self.con = 0x40
            elif d == 0x24: # start
                self.con = 0x48
                self.stat = 0x10
            elif d == 0x10: # stop
                if (self.addr & 0x01) == 0x00:
                    self.mcu.I2CWrite_Callback(self.addr, self.i2c_data)
            else:
                assert 0
            logging.info("P: LPC_I2C->CONSET << %08x", d)
        elif (a&0xff) == 0x08: # DAT
            if self.stat == 0x10: # start
                self.addr = d & 0xff
                self.i2c_data = []
                if self.addr & 0x01:
                    self.i2c_data = self.mcu.I2CRead_Callback(self.addr)
                    self.stat = 0x40
                    self.pos = 0
                else:
                    self.stat = 0x18
            else:
                self.i2c_data.append(d & 0xff)
                self.stat = 0x18
            logging.info("P: LPC_I2C->DAT << %08x", d)
        elif (a&0xff) == 0x10:
            logging.info("P: LPC_I2C->SCLH << %08x", d)
        elif (a&0xff) == 0x14:
            logging.info("P: LPC_I2C->SCLL << %08x", d)
        elif (a&0xff) == 0x18:
            logging.info("P: LPC_I2C->CONCLR << %08x", d)
        else:
            logging.warn("P: LPC_I2C %08x << %08x", a, d)

    def peek32(self, a):
        d = 0x00000000
        if (a&0xff) == 0x00:
            d = self.con
            logging.info("P: LPC_I2C->CONSET >> %08x", d)
        elif (a&0xff) == 0x04:
            d = self.stat
            logging.info("P: LPC_I2C->STAT >> %08x", d)
        elif (a&0xff) == 0x08:
            d = self.i2c_data[self.pos]
            self.pos += 1
            if self.pos < len(self.i2c_data):
                self.stat = 0x50
            else:
                self.stat = 0x58
            logging.info("P: LPC_I2C->DAT >> %08x", d)
        else:
            logging.warn("P: LPC_I2C %08x >> %08x", a, d)
        return d

class EMU111x_SPI:
    def __init__(self, mcu, ch):
        assert ch == 0 or ch == 1
        self.mcu = mcu
        self.ch = ch
        self.cr0 = 0
        self.cr1 = 0
        self.dly = 0
        self.dr = 0

    def poke32(self, a, d):
        if (a&0xff) == 0x00:
            self.cr0 = d
            logging.info("P: LPC_SPI%d->CR0 << %08x", self.ch, d)
        elif (a&0xff) == 0x04:
            self.cr1 = d
            logging.info("P: LPC_SPI%d->CR1 << %08x", self.ch, d)
        elif (a&0xff) == 0x08:
            self.dr = self.mcu.SPIWrite_Callback(self.ch, d)
            logging.info("P: LPC_SPI%d->DR << %08x", self.ch, d)
        elif (a&0xff) == 0x10:
            logging.info("P: LPC_SPI%d->CPSR << %08x", self.ch, d)
        else:
            logging.warn("P: LPC_SPI%d %08x << %08x", self.ch, a, d)

    def peek32(self, a):
        d = 0x00000000
        if (a&0xff) == 0x00:
            d = self.cr0
            logging.info("P: LPC_SPI%d->CR0 >> %08x", self.ch, d)
        elif (a&0xff) == 0x04:
            d = self.cr1
            logging.info("P: LPC_SPI%d->CR1 >> %08x", self.ch, d)
        elif (a&0xff) == 0x08:
            d = self.dr
            logging.info("P: LPC_SPI%d->DR >> %08x", self.ch, d)
        elif (a&0xff) == 0x0c:
            d = 0x06
            logging.info("P: LPC_SPI%d->SR >> %08x", self.ch, d)
        else:
            logging.warn("P: LPC_SPI%d %08x >> %08x", self.ch, a, d)
        return d

class EMU111x_NVIC:
    def __init__(self):
        self.iser = 0x00
    def poke32(self, a, d):
        if (a&0xfff) == 0x100:
            self.iser |= d
            logging.info("P: NVIC->ISER[0] << %08x", d)
        elif (a&0xfff) == 0x180:
            logging.info("P: NVIC->ICER[0] << %08x", d)
        else:
            logging.warn("P: NVIC %08x << %08x", a, d)

class EMU111x_SCB:
    def __init__(self):
        self.vtor = 0

    def poke32(self, a, d):
        if (a&0xfff) == 0xd08:
            self.vtor = d
            logging.info("P: SCB->VTOR << %08x", d)
        else:
            logging.warn("P: SCB %08x << %08x", a, d)

    def peek32(self, a):
        d = 0
        if (a&0xfff) == 0xd08:
            d = self.vtor
            logging.info("P: SCB->VTOR >> %08x", d)
        else:
            logging.warn("P: SCB %08x >> %08x", a, d)
        return d

class EMU111x(BaseV6M.BaseV6M):
    def __init__(self, flash_size, ram_size):
        super(EMU111x, self).__init__()
        self.flash_size = flash_size
        self.ram_size = ram_size
        self.flash = [0] * self.flash_size
        self.ram = [0] * self.ram_size
        self.rom = [0] * EMU111x_ROM_SIZE
        self.syscon = EMU111x_SYSCON()
        self.iocon = EMU111x_IOCON()
        self.gpio0 = EMU111x_GPIO(self, 0)
        self.gpio1 = EMU111x_GPIO(self, 1)
        self.gpio2 = EMU111x_GPIO(self, 2)
        self.tmr32b1 = EMU111x_TMR32B(1)
        self.uart = EMU111x_UART(self)
        self.i2c = EMU111x_I2C(self)
        self.spi0 = EMU111x_SPI(self, 0)
        self.spi1 = EMU111x_SPI(self, 1)
        self.scb = EMU111x_SCB()
        self.nvic = EMU111x_NVIC()
        self.SerialPutc_Callback = lambda ch,c : None
        self.SerialGetc_Callback = lambda ch: -1
        self.SerialReadable_Callback = lambda ch: 0
        self.DigitalWrite_Callback = lambda port,pin, value: value
        self.DigitalRead_Callback = lambda port,pin: 0
        self.I2CWrite_Callback = lambda addr,data: None
        self.I2CRead_Callback = lambda addr: None
        self.SPIWrite_Callback = lambda ch,value: 0

    def poke32(self, a, d):
        if a>>24 == EMU111x_RAM_BASE>>24:
            assert EMU111x_RAM_BASE <= a < EMU111x_RAM_BASE+self.ram_size
            self.ram[a - EMU111x_RAM_BASE + 0] = (d>>0) & 0xff
            self.ram[a - EMU111x_RAM_BASE + 1] = (d>>8) & 0xff
            self.ram[a - EMU111x_RAM_BASE + 2] = (d>>16) & 0xff
            self.ram[a - EMU111x_RAM_BASE + 3] = (d>>24) & 0xff
            logging.info("W: %08x %08x", a, d)
        elif a>>24 == EMU111x_APB_BASE>>24:
            if   (a>>12)&0xff == 0x00: self.i2c.poke32(a, d)
            elif (a>>12)&0xff == 0x08: self.uart.poke32(a, d)
            elif (a>>12)&0xff == 0x18: self.tmr32b1.poke32(a, d)
            elif (a>>12)&0xff == 0x40: self.spi0.poke32(a, d)
            elif (a>>12)&0xff == 0x44: self.iocon.poke32(a, d)
            elif (a>>12)&0xff == 0x48: self.syscon.poke32(a, d)
            elif (a>>12)&0xff == 0x58: self.spi1.poke32(a, d)
            else:
                logging.warn("P: %08x %08x", a, d)
        elif a>>24 == EMU111x_AHB_BASE>>24:
            if (a>>16)&0xf == 0:
                self.gpio0.poke32(a, d)
            elif (a>>16)&0xf == 1:
                self.gpio1.poke32(a, d)
            elif (a>>16)&0xf == 2:
                self.gpio2.poke32(a, d)
            else:
                logging.warn("P: %08x << %08x", a, d)
        elif a>>24 == 0xe0:
            if 0xe000e100 <= a <= 0xe000e180:
                self.nvic.poke32(a, d)
            elif a == 0xe000ed08:
                self.scb.poke32(a, d)
            else:
                logging.warn("P: %08x %08x", a, d)
        elif a>>24 == EMU111x_FLASH_BASE>>24:
            self.trace_warn()
            assert 0, "%08x" % a
        elif a>>24 == EMU111x_ROM_BASE>>24:
            self.trace_warn()
            assert 0, "%08x" % a
        else:
            self.trace_warn()
            assert 0, "%08x" % a

    def poke8(self, a, d):
        if a>>24 == EMU111x_RAM_BASE>>24:
            assert EMU111x_RAM_BASE <= a < EMU111x_RAM_BASE+self.ram_size
            self.ram[a - EMU111x_RAM_BASE] = d
            logging.info("W: %08x %02x", a, d)
        else:
            self.trace_warn()
            assert 0, "%08x" % a

    def peek32(self, a):
        d = 0x00000000
        if a>>24 == EMU111x_RAM_BASE>>24:
            assert EMU111x_RAM_BASE <= a < EMU111x_RAM_BASE+self.ram_size
            d = self.ram[a - EMU111x_RAM_BASE]
            d |= self.ram[a - EMU111x_RAM_BASE + 1]<<8
            d |= self.ram[a - EMU111x_RAM_BASE + 2]<<16
            d |= self.ram[a - EMU111x_RAM_BASE + 3]<<24
            logging.info("R: %08x %08x", a, d)
        elif a>>24 == EMU111x_FLASH_BASE>>24:
            assert EMU111x_FLASH_BASE <= a < EMU111x_FLASH_BASE+self.flash_size
            d = self.flash[a - EMU111x_FLASH_BASE]
            d |= self.flash[a - EMU111x_FLASH_BASE + 1]<<8
            d |= self.flash[a - EMU111x_FLASH_BASE + 2]<<16
            d |= self.flash[a - EMU111x_FLASH_BASE + 3]<<24
            logging.info("R: %08x %08x", a, d)
        elif a>>24 == EMU111x_ROM_BASE>>24:
            assert EMU111x_ROM_BASE <= a < EMU111x_ROM_BASE+EMU111x_ROM_SIZE
            d = self.rom[a - EMU111x_ROM_BASE]
            d |= self.rom[a - EMU111x_ROM_BASE + 1]<<8
            d |= self.rom[a - EMU111x_ROM_BASE + 2]<<16
            d |= self.rom[a - EMU111x_ROM_BASE + 3]<<24
            logging.info("R: %08x %08x", a, d)
        elif a>>24 == EMU111x_APB_BASE>>24:
            if   (a>>12)&0xff == 0x00: d = self.i2c.peek32(a)
            elif (a>>12)&0xff == 0x08: d = self.uart.peek32(a)
            elif (a>>12)&0xff == 0x18: d = self.tmr32b1.peek32(a)
            elif (a>>12)&0xff == 0x40: d = self.spi0.peek32(a)
            elif (a>>12)&0xff == 0x44: d = self.iocon.peek32(a)
            elif (a>>12)&0xff == 0x48: d = self.syscon.peek32(a)
            elif (a>>12)&0xff == 0x58: d = self.spi1.peek32(a)
            else:
                logging.info("P: %08x", a)
        elif a>>24 == EMU111x_AHB_BASE>>24:
            if (a>>16)&0xf == 0:
                d = self.gpio0.peek32(a)
            elif (a>>16)&0xf == 1:
                d = self.gpio1.peek32(a)
            elif (a>>16)&0xf == 2:
                d = self.gpio2.poke32(a)
            else:
                logging.warn("P: %08x >> %08x", a, d)
        elif a>>24 == 0xe0:
            if  a == 0xe000ed08:
                d = self.scb.peek32(a)
            else:
                logging.warn("P: %08x", a)
        else:
            self.trace_warn()
            assert 0, "%08x" % a
        return d

    def peek8(self, a):
        d = 0x00
        if a>>24 == EMU111x_RAM_BASE>>24:
            assert EMU111x_RAM_BASE <= a < EMU111x_RAM_BASE+self.ram_size
            d = self.ram[a - EMU111x_RAM_BASE]
            logging.info("R: %08x %02x", a, d)
        elif a>>24 == EMU111x_FLASH_BASE>>24:
            assert EMU111x_FLASH_BASE <= a < EMU111x_FLASH_BASE+self.flash_size
            d = self.flash[a - EMU111x_FLASH_BASE]
            logging.info("R: %08x %02x", a, d)
        elif a>>24 == EMU111x_ROM_BASE>>24:
            assert EMU111x_ROM_BASE <= a < EMU111x_ROM_BASE+EMU111x_ROM_SIZE
            d = self.rom[a - EMU111x_ROM_BASE]
            logging.info("R: %08x %02x", a, d)
        else:
            self.trace_warn()
            assert 0, "%08x" % a
        return d

    def clock_in(self, n):
        self.tmr32b1.clock_in(n)

    def trace(self):
        logging.info("S: r0=%08x r1=%08x  r2=%08x  r3=%08x  r4=%08x r5=%08x r6=%08x r7=%08x", self.R[0], self.R[1], self.R[2], self.R[3], self.R[4], self.R[5], self.R[6], self.R[7])
        logging.info("S: r8=%08x r9=%08x r10=%08x r11=%08x r12=%08x sp=%08x lr=%08x pc=%08x", self.R[8], self.R[9], self.R[10], self.R[11], self.R[12], self.R[13], self.R[14], self.R[15])
        logging.info("S: xPSR=%08x N=%d Z=%d C=%d V=%d", self.R[16], self.N(), self.Z(), self.C(), self.V())
        logging.debug("S: cycle=%d code=%02x code2nd=%02x im=%08x d=%d n=%d m=%d", self.cycle, self.code, self.code2nd, self.R[17], self.d, self.n, self.m)

    def trace_warn(self):
        logging.warn("S: r0=%08x r1=%08x  r2=%08x  r3=%08x  r4=%08x r5=%08x r6=%08x r7=%08x", self.R[0], self.R[1], self.R[2], self.R[3], self.R[4], self.R[5], self.R[6], self.R[7])
        logging.warn("S: r8=%08x r9=%08x r10=%08x r11=%08x r12=%08x sp=%08x lr=%08x pc=%08x", self.R[8], self.R[9], self.R[10], self.R[11], self.R[12], self.R[13], self.R[14], self.R[15])
        logging.warn("S: xPSR=%08x N=%d Z=%d C=%d V=%d", self.R[16], self.N(), self.Z(), self.C(), self.V())
        logging.warn("S: cycle=%d code=%02x code2nd=%02x im=%08x d=%d n=%d m=%d", self.cycle, self.code, self.code2nd, self.R[17], self.d, self.n, self.m)

    def load_flash(self, filename):
        with open(filename, "rb") as f:
            for i,c in enumerate(f.read()):
                self.flash[i] = ord(c)

    def load_rom(self, filename):
        with open(filename, "rb") as f:
            for i,c in enumerate(f.read()):
                self.rom[i] = ord(c)

