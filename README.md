# emu810 #

LPC810 emulator pre-alpha version

### usage ###

emu810.py lpc810_helloworld_LPC812.bin

### link ###

* https://developer.mbed.org/users/va009039/code/lpc810_helloworld/
* https://developer.mbed.org/users/va009039/code/emu812/
* https://developer.mbed.org/users/va009039/code/emu1114/