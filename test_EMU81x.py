# test_EMU81x.py 2015/8/7
# coding:utf-8
#
import sys
import logging
import unittest
import EMU81x

SUCCESS_KEY = "success"
FAIL_KEY = "fail"

class Test_EMU81x(unittest.TestCase):
    def setUp(self):
        self.mcu = EMU81x.EMU81x(flash_size=4*1024, ram_size=1*1024)
        self.mcu.SerialPutc_Callback = self.SerialPutc_Callback
        self.mcu.SerialGetc_Callback = self.SerialGetc_Callback
        self.mcu.SerialReadable_Callback = self.SerialReadable_Callback
        self.mcu.DigitalWrite_Callback = self.DigitalWrite_Callback
        self.mcu.I2CWrite_Callback = self.I2CWrite_Callback
        self.mcu.I2CRead_Callback = self.I2CRead_Callback
        self.mcu.SPIWrite_Callback = self.SPIWrite_Callback
        self.uart_output = [""]*3
        self.input = ""
        self.prev_ch = -1
        self.loopback = False
        self.success = False
        self.fail = False
        self.PCA9672_addr = (0x23<<1)
        self.PCA9672_data = 0x00

    def tearDown(self):
        pass

    def mcu_run(self):
        clk = 0
        for _ in xrange(50000):
            self.mcu.fetch()
            self.mcu.execute()
            clk += 1
            if clk > 20:
                self.mcu.clock_in(1000000)
                clk = 0
            if self.success or self.fail:
                break

    def SerialPutc_Callback(self, ch, c):
        if ch != self.prev_ch:
            sys.stdout.write("*** ch=%d: " % ch)
            self.prev_ch = ch
        sys.stdout.write(c)
        self.uart_output[ch] += c
        output = self.uart_output[ch]
        if output.find(SUCCESS_KEY) >= 0:
            self.success = True
        elif output.find(FAIL_KEY) >= 0:
            self.fail = True
        if self.loopback:
            self.input += c

    def SerialReadable_Callback(self, ch):
        if ch == 0:
            if len(self.input) > 0:
                return 1
            else:
                return 0
        return 0

    def SerialGetc_Callback(self, ch):
        assert ch == 0
        c = self.input[0]
        self.input = self.input[1:]
        return c

    def DigitalWrite_Callback(self, port, pin, value):
        print "*** DigitalWrite(%s, %d)" % (pin, value)

    def DigitalRead_Callback(self, port, pin):
        return 0

    def I2CWrite_Callback(self, addr, data):
        print "*** I2CWrite(addr=%02x, %s)" % (addr, map(lambda x:"0x%02x" % x, data))
        return 0

    def I2CRead_Callback(self, addr):
        if (addr&0xfe) == self.PCA9672_addr:
            data = [self.PCA9672_data]
            self.PCA9672_data ^= 0x80
        else:
            data = [0x00]
        print "*** I2CRead(addr=%02x, %s)" % (addr, map(lambda x:"0x%02x" % x, data))
        return data

    def SPIWrite_Callback(self, ch, value):
        data = value + 1;
        print "*** SPIWrite(ch=%d, wr:%02x) rd:%02x" % (ch, value, data)
        return data

    def test_rom_i2c(self):
        self.mcu.load_flash(r"test_rom_i2c_LPC812.bin")
        self.mcu.load_rom(r"LPC812.ROM")
        self.mcu.reset()
        self.mcu_run()
        self.assertTrue(self.success)

    def test_rom_uart(self):
        self.mcu.load_flash(r"test_rom_uart_LPC812.bin")
        self.mcu.load_rom(r"LPC812.ROM")
        self.mcu.reset()
        clk = 0
        for _ in xrange(50000):
            self.mcu.fetch()
            self.mcu.execute()
            clk += 1
            if clk > 20:
                self.mcu.clock_in(1000000)
                clk = 0
            if self.success or self.fail:
                break
        self.assertTrue(self.success)

    def test_spi(self):
        filename = r"test_spi_LPC812.bin"
        self.mcu.load_flash(filename)
        self.mcu.reset()
        ck = 0
        for _ in xrange(50000):
            self.mcu.fetch()
            self.mcu.execute()
            ck += 1
            if ck > 20:
                self.mcu.clock_in(1000000)
                ck = 0
            if self.success or self.fail:
                break
        self.assertTrue(self.success)

    def test_i2c(self):
        filename = r"test_i2c_LPC812.bin"
        self.mcu.load_flash(filename)
        self.mcu.reset()
        ck = 0
        for _ in xrange(50000):
            self.mcu.fetch()
            self.mcu.execute()
            ck += 1
            if ck > 20:
                self.mcu.clock_in(1000000)
                ck = 0
            if self.success or self.fail:
                break
        self.assertTrue(self.success)

    def test_gpio(self):
        filename = r"test_gpio_LPC812.bin"
        self.mcu.load_flash(filename)
        self.mcu.reset()
        ck = 0
        for _ in xrange(50000):
            self.mcu.fetch()
            self.mcu.execute()
            ck += 1
            if ck > 20:
                self.mcu.clock_in(1000000)
                ck = 0
            if self.success or self.fail:
                break
        self.assertTrue(self.success)

    def test_serial(self):
        #self.loopback = True
        filename = r"test_serial_LPC812.bin"
        self.mcu.load_flash(filename)
        self.mcu.reset()
        ck = 0
        for _ in xrange(50000):
            self.mcu.fetch()
            self.mcu.execute()
            ck += 1
            if ck > 20:
                self.mcu.clock_in(1000000)
                ck = 0
            if self.success or self.fail:
                break
        self.assertTrue(self.success)

if __name__=="__main__":
    #logging.basicConfig(level=logging.INFO)
    #logging.basicConfig(level=logging.INFO, filename="R:/test_v6m.log")
    unittest.main()


