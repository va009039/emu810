# test_v6m.py 2015/8/6
# coding:utf-8
#
import random
import logging
import unittest
import BaseV6M

class MyV6M(BaseV6M.BaseV6M):
    def __init__(self):
        super(MyV6M, self).__init__()
        self.mem = {}
    def poke32(self, a, d):
        self.poke8(a, d & 0xff)
        self.poke8(a+1, (d>>8)&0xff)
        self.poke8(a+2, (d>>16)&0xff)
        self.poke8(a+3, (d>>24)&0xff)
    def poke8(self, a, d):
        self.mem[a] = d
    def peek32(self, a):
        return self.peek8(a)|self.peek8(a+1)<<8|self.peek8(a+2)<<16|self.peek8(a+3)<<24
    def peek8(self, a):
        if a in self.mem:
            return self.mem[a]
        return 0x00
    def trace(self, a):
        pass

class Test_BaseV6M(unittest.TestCase):
    def setUp(self):
        self.v6m = MyV6M()
        self.v6m.reset()

    def tearDown(self):
        pass

    def code_exec(self, pc, code, code2nd=0):
        if code:
            self.v6m.mem[pc] = code&0xff
            self.v6m.mem[pc+1] = code>>8
            self.v6m.mem[pc+2] = code2nd&0xff
            self.v6m.mem[pc+3] = code2nd>>8
        self.v6m.jump(pc)
        self.v6m.fetch()
        self.v6m.execute()

    def code_test(self, data, ip = 0x1000):
        for (source, result) in data:
            reg_index = {'r0': 0, 'r1': 1, 'r2': 2, 'r3': 3, 'r4': 4, 'r5': 5, 'r6': 6, 'r7': 7}
            for (source, result) in data:
                for k,v in source.iteritems():
                    if k in ["code"]:
                        code = v
                    elif k in ["C"]:
                        self.v6m.Cin(v)
                    elif k in reg_index:
                        self.v6m.R[reg_index[k]] = v
                    else:
                        self.v6m.mem[k] = v

            self.code_exec(ip, code)

            for k,v in result.iteritems():
                if 0: pass
                elif k == 'r0': self.assertEqual(self.v6m.R[0], v)
                elif k == 'r1': self.assertEqual(self.v6m.R[1], v)
                elif k == 'r2': self.assertEqual(self.v6m.R[2], v)
                elif k == 'r3': self.assertEqual(self.v6m.R[3], v)
                elif k == 'r4': self.assertEqual(self.v6m.R[4], v)
                elif k == 'r5': self.assertEqual(self.v6m.R[5], v)
                elif k == 'r6': self.assertEqual(self.v6m.R[6], v)
                elif k == 'r7': self.assertEqual(self.v6m.R[7], v)
                elif k == 'N': self.assertEqual(self.v6m.N(), v)
                elif k == 'Z': self.assertEqual(self.v6m.Z(), v)
                elif k == 'C': self.assertEqual(self.v6m.C(), v)
                elif k == 'V': self.assertEqual(self.v6m.V(), v)
                else: self.assertEqual(self.v6m.mem[k], v)

    def test_c_ldr_pc(self): # LDR Rd,[pc,#imm8] 0x4800-0x4fff
        for _ in xrange(1):
            data = [
                ({'code': 0x4800, 'r0': 0x00}, {0x1000: 0x00, 0x1001: 0x48, 'r0': 0x00}), # LDR R0,[pc,#0]
                ({'code': 0x4801, 'r0': 0x00}, {'r0': 0x00}), # LDR R0,[pc,#4]
                ({'code': 0x4800, 0x1004: 0x12, 0x1005: 0x34, 0x1006: 0x56, 0x1007: 0x78}, {'r0': 0x78563412}), # LDR R0,[pc,#0]
                ({'code': 0x4900, 0x1004: 0x78, 0x1005: 0x56, 0x1006: 0x34, 0x1007: 0x12}, {'r1': 0x12345678}), # LDR R1,[pc,#0]
                ({'code': 0x4a01, 0x1008: 0x23, 0x1009: 0x45, 0x100a: 0x67, 0x100b: 0x89}, {'r2': 0x89674523}), # LDR R2,[pc,#4]
                ({'code': 0x4b02, 0x100c: 0x45, 0x100d: 0x45, 0x100e: 0x67, 0x100f: 0x89}, {'r3': 0x89674545}), # LDR R3,[pc,#8]
                ({'code': 0x4c03, 0x1010: 0x89, 0x1011: 0x67, 0x1012: 0x45, 0x1013: 0x23}, {'r4': 0x23456789}), # LDR R4,[pc,#12]
                ({'code': 0x4d04, 0x1014: 0x90, 0x1015: 0x78, 0x1016: 0x56, 0x1017: 0x34}, {'r5': 0x34567890}), # LDR R5,[pc,#16]
                ({'code': 0x4e05, 0x1018: 0x01, 0x1019: 0x89, 0x101a: 0x67, 0x101b: 0x45}, {'r6': 0x45678901}), # LDR R6,[pc,#20]
                ({'code': 0x4f06, 0x101c: 0x12, 0x101d: 0x90, 0x101e: 0x78, 0x101f: 0x56}, {'r7': 0x56789012}), # LDR R7,[pc,#24]
            ]
            self.code_test(data, 0x1000)

    def test_c_rev(self):
        for _ in xrange(1):
            data = [
                ({'code': 0xba00, 'r0': 0x00000000}, {'r0': 0x00000000, }), # REV r0,r0
                ({'code': 0xba00, 'r0': 0x12345678}, {'r0': 0x78563412, }), # REV r0,r0
                ({'code': 0xba01, 'r0': 0x12345678}, {'r1': 0x78563412, }), # REV r1,r0
                ({'code': 0xba40, 'r0': 0x00000000}, {'r0': 0x00000000, }), # REV16 r0,r0
                ({'code': 0xba40, 'r0': 0x12345678}, {'r0': 0x34127856, }), # REV16 r0,r0
                ({'code': 0xba41, 'r0': 0x12345678}, {'r1': 0x34127856, }), # REV16 r1,r0
                ({'code': 0xbac0, 'r0': 0x00000000}, {'r0': 0x00000000, }), # REVSH r0,r0
                ({'code': 0xbac1, 'r0': 0x00000000}, {'r1': 0x00000000, }), # REVSH r1,r0
                ({'code': 0xbac1, 'r0': 0x12345678}, {'r1': 0x00007856, }), # REVSH r1,r0
                ({'code': 0xbac1, 'r0': 0x23456789}, {'r1': 0xffff8967, }), # REVSH r1,r0
            ]
            self.code_test(data)

    def test_c_sub(self):
        for _ in xrange(1):
            data = [
                ({'code': 0x1a00, 'r0': 0x00}, {'r0': 0x00, 'N':0, 'Z':1, 'C':1}), # SUB r0,r0,r0
                ({'code': 0x1a01, 'r0': 0x00, 'r1': 0x00}, {'r0': 0x00, 'r1': 0x00, 'N':0, 'Z':1, 'C':1}), # SUB r1,r0,r0
            ]
            self.code_test(data)

    def test_c_op_0(self):
        for _ in xrange(1):
            data = [
                ({'code': 0x4200, 'r0': 0x00}, {'r0': 0, 'N':0, 'Z':1, }), # TST r0,r0; r0=0

                ({'code': 0x4180, 'r0': 0x00, 'C': 1}, {'r0': 0x00, 'C':1}), # SBC r0,r0; r0=0,C=1
                ({'code': 0x4181, 'r0': 0x02, 'r1': 0x00, 'C': 1}, {'r0': 0x02, 'C':0}), # SBC r1,r0; r0=0,C=1
            ]
            self.code_test(data)

    def test_c_op_1(self):
        for _ in xrange(1):
            data = [
                ({'code': 0x4140, 'r0': 0x00, 'C': 0}, {'r0': 0x00, 'C':0}), # ADC r0,r0; r0=0,C=0

                ({'code': 0x40c0, 'r0': 0x00}, {'r0': 0x00}), # LSR r0,r0; r0=0

                ({'code': 0x4080, 'r0': 0x00}, {'r0': 0x00}), # LSL r0,r0; r0=0

                ({'code': 0x4100, 'r0': 0x00}, {'r0': 0x00}), # ASR r0,r0; r0=0
                ({'code': 0x4101, 'r0': 0, 'r1':0x01}, {'r0': 0, 'r1':0x01}), # ASR r1,r0
                ({'code': 0x4101, 'r0': 1, 'r1':0x01}, {'r0': 1, 'r1':0x00}), # ASR r1,r0

                ({'code': 0x4380, 'r0': 0x00}, {'r0': 0x00}), # BIC r0,r0; r0=0
                ({'code': 0x4380, 'r0': 0x0000ffff}, {'r0': 0x00000000}), # BIC r0,r0; r0=0
                ({'code': 0x4381, 'r0': 0xffff0000, 'r1':0xffffffff}, {'r1': 0x0000ffff}), # BIC r1,r0; r0=0

                ({'code': 0x42c0, 'r0': 0x0}, {'N': 0, 'Z': 1, 'C': 0, 'V':0 }), # CMN r0,r0; r0=0

                ({'code': 0x4280, 'r0': 0x0}, {'r0': 0x0, 'Z': 1 }), # CMP r0,r0; r0=0
                ({'code': 0x4281, 'r0': 0x00, 'r1':0x00}, {'Z': 1}), # CMP r1,r0

                ({'code': 0x4000, 'r0': 0x0}, {'r0': 0x0}), # AND r0,r0; r0=0
                ({'code': 0x4001, 'r0': 0x01, 'r1': 0x01}, {'r1': 0x01}), # AND r1,r0

                ({'code': 0x4040, 'r0': 0x0}, {'r0': 0x0}), # EORS r0,r0; r0=0
                ({'code': 0x4040, 'r0': 0xff}, {'r0': 0x00}), # EORS r0,r0; r0=0xff
                ({'code': 0x4041, 'r0': 0x01, 'r1': 0x01}, {'r1': 0x00}), # EORS r1,r0

                ({'code': 0x41c0, 'r0': 0x0}, {'r0': 0x0}), # ROR r0,r0; r0=0
                ({'code': 0x41c1, 'r0': 0x1, 'r1':0x00}, {'r0': 0x1, 'r1':0x0}), # ROR r1,r0; r0=1,r1=0

                ({'code': 0x4240, 'r0': 0x00000000}, {'r0': 0x00000000}), # NEG r0,r0 ; r0=0
                ({'code': 0x4240, 'r0': 0x00000001}, {'r0': 0xffffffff}), # NEG r0,r0 ; r0=1
                ({'code': 0x4240, 'r0': 0x00000002}, {'r0': 0xfffffffe}), # NEG r0,r0 ; r0=2
                ({'code': 0x4240, 'r0': 0xffffffff}, {'r0': 0x00000001}), # NEG r0,r0 ; r0=-1

                ({'code': 0x4340, 'r0': 0x0}, {'r0': 0x0}), # MUL r0,r0; r0=0

            ]
            self.code_test(data)

    def test_e_mul(self):
        for _ in xrange(1):
            data = [
                ({'code': 0x4340, 'r0': 0x00}, {'r0': 0x00}), # MUL r0,r0; r0=0
                ({'code': 0x4341, 'r0': 0x00, 'r1': 0x00}, {'r0': 0x00}), # MUL r1,r0
                ({'code': 0x4341, 'r0': 0x01, 'r1': 0x01}, {'r1': 0x01}), # MUL r1,r0
                ({'code': 0x4341, 'r0': 0x02, 'r1': 0x03}, {'r1': 0x06}), # MUL r1,r0
            ]
            self.code_test(data)

    def test_e_mvn(self):
        for _ in xrange(1):
            data = [
                ({'code': 0x43c0, 'r0': 0x00000000}, {'r0': 0xffffffff}),
                ({'code': 0x43c0, 'r0': 0x00000001}, {'r0': 0xfffffffe}),
                ({'code': 0x43c0, 'r0': 0x00000002}, {'r0': 0xfffffffb}),
                ({'code': 0x43c0, 'r0': 0xffffffff}, {'r0': 0x00000000}),
            ]
            self.code_test(data)

    def test_c_op(self):
        for _ in xrange(1):
            for code in xrange(0x4000, 0x43ff+1):
                m = (code>>3)&0x07
                Rm = random.randint(0, 0xffffffff)
                self.v6m.R[m] = Rm
                d = code&0x07
                if d == m:
                    Rd = Rm
                else:
                    Rd = random.randint(0, 0xffffffff)
                self.v6m.R[d] = Rd
                Carry = random.randint(0, 1)
                self.v6m.Cin(Carry)
                
                self.code_exec(0x1000, code)

                if (code&0xffc0) == 0x4000: # AND
                    result = Rd & Rm
                    self.assertEqual(self.v6m.R[d], result)

                if (code&0xffc0) == 0x4040: # EOR
                    result = Rd ^ Rm
                    self.assertEqual(self.v6m.R[d], result)

                if (code&0xffc0) == 0x4080: # LSL
                    s = Rm & 0xff
                    result = Rd << s
                    self.assertEqual(self.v6m.R[d], result & 0xffffffff)
                    if result & 0x100000000:
                        self.assertEqual(self.v6m.C(), 1)
                    else:
                        self.assertEqual(self.v6m.C(), 0)

                if (code&0xffc0) == 0x40c0: # LSR
                    s = Rm & 0xff
                    result = Rd >> s
                    self.assertEqual(self.v6m.R[d], result)

                if (code&0xffc0) == 0x4100: # ASR
                    s = Rm & 0xff
                    result = Rd
                    for _ in range(s):
                        if result & 0x80000000:
                            result >>= 1
                            result |= 0x80000000
                        else:
                            result >>= 1
                    self.assertEqual(self.v6m.R[d], result)

                if (code&0xffc0) == 0x4140: # ADC
                    result = Rd + Rm + Carry
                    self.assertEqual(self.v6m.R[d], result & 0xffffffff)
                    if result & 0x100000000:
                        self.assertEqual(self.v6m.C(), 1)
                    else:
                        self.assertEqual(self.v6m.C(), 0)

                if (code&0xffc0) == 0x4180: # SBC
                    result = Rd - Rm - (1-Carry)
                    self.assertEqual(self.v6m.R[d], result & 0xffffffff)

                if (code&0xffc0) == 0x41c0: # ROR
                    result = Rd
                    for _ in xrange(Rm & 0xff):
                        if result & 1:
                            result >>= 1
                            result |= 1<<31
                        else:
                            result >>= 1
                    self.assertEqual(self.v6m.R[d], result)

                if (code&0xffc0) == 0x4200: # TST
                    result = Rd & Rm
                    if result == 0:
                        self.assertEqual(self.v6m.Z(), 1)
                    else:
                        self.assertEqual(self.v6m.Z(), 0)
                    self.assertEqual(self.v6m.R[d], Rd)
                    self.assertEqual(self.v6m.R[m], Rm)

                if (code&0xffc0) == 0x4240: # NEG
                    result = (0 - Rm) & 0xffffffff
                    self.assertEqual(self.v6m.R[d], result)

                if (code&0xffc0) == 0x4280: # CMP
                    if Rd >= Rm:
                        self.assertEqual(self.v6m.C(), 1)
                    else:
                        self.assertEqual(self.v6m.C(), 0)
                    if Rd == Rm:
                        self.assertEqual(self.v6m.Z(), 1)
                    else:
                        self.assertEqual(self.v6m.Z(), 0)

                if (code&0xffc0) == 0x42c0: # CMN
                    result = Rd + Rm
                    if result & 0x100000000:
                        self.assertEqual(self.v6m.C(), 1)
                    else:                        
                        self.assertEqual(self.v6m.C(), 0)

                if (code&0xffc0) == 0x4300: # OR
                    result = Rd | Rm
                    self.assertEqual(self.v6m.R[d], result)                

                if (code&0xffc0) == 0x4340: # MUL
                    result = (Rd * Rm) & 0xffffffff
                    self.assertEqual(self.v6m.R[d], result)

                if (code&0xffc0) == 0x4380: # BIC
                    result = (Rd & ~Rm) & 0xffffffff
                    self.assertEqual(self.v6m.R[d], result)

                if (code&0xffc0) == 0x43c0: # MVN
                    result = (~Rm) & 0xffffffff
                    self.assertEqual(self.v6m.R[d], result)


    def test_c_lsl(self):
        for _ in xrange(3):
            for code in xrange(0x0000, 0x07ff+1):
                n = (code>>3)&0x07
                Rn = random.randint(0, 0xffffffff)
                self.v6m.R[n] = Rn
                self.code_exec(0x1000, code)
                d = code&0x07
                s = (code>>6)&0x1f
                result = Rn << s
                Carry = (result>>32)&1
                self.assertEqual(self.v6m.R[d], result & 0xffffffff)
                self.assertEqual(self.v6m.C(), Carry)

    def test_c_lsr(self):
        for _ in xrange(3):
            for code in xrange(0x0800, 0x0fff+1):
                n = (code>>3)&0x07
                Rn = random.randint(0, 0xffffffff)
                self.v6m.R[n] = Rn
                self.code_exec(0x1000, code)
                d = code&0x07
                s = (code>>6)&0x1f
                if s == 0:
                    s = 32
                result = Rn >> s
                Carry = (Rn >> (s-1))&1
                self.assertEqual(self.v6m.R[d], result)
                self.assertEqual(self.v6m.C(), Carry)

    def test_c_asr(self):
        for _ in xrange(3):
            for code in xrange(0x1000, 0x17ff+1):
                n = (code>>3)&0x07
                Rn = random.randint(0, 0xffffffff)
                self.v6m.R[n] = Rn
                self.code_exec(0x1000, code)
                d = code&0x07
                s = (code>>6)&0x1f
                if s == 0:
                    s = 32
                msb = Rn & 0x80000000
                result = Rn
                for _ in range(s):
                    Carry = result & 1
                    result >>= 1
                    result |= msb
                self.assertEqual(self.v6m.R[d], result)
                self.assertEqual(self.v6m.C(), Carry)

    def test_e_ror(self):
        for _ in xrange(3):
            for code in xrange(0x43c00, 0x43ff+1):
                d = code&0x07
                Rd = random.randint(0, 0xffffffff)
                self.v6m.R[d] = Rd
                self.v6m.Cin(0)
                self.code_exec(0x1000, code)
                s = (code>>3)&0x07
                if s == 0:
                    s = 32
                result = Rn
                Carry = 0
                for _ in range(s):
                    Carry = result & 1
                    msb = result<<31
                    result >>= 1
                    result |= msb
                self.assertEqual(self.v6m.R[d], result)
                self.assertEqual(self.v6m.C(), Carry)



    def test_c_b(self):
        for code in xrange(0xe000, 0xe7ff+1):
            pc = 0x1000
            self.code_exec(pc, code)
            pc += 4
            pc += BaseV6M.signed_immed(code, 11) * 2
            self.assertEqual(self.v6m.R[15], pc)

    def test_c_b_forward(self):
        for code in xrange(0xe000, 0xe3ff+1):
            pc = 0x1000
            self.code_exec(pc, code)
            pc += 4
            pc += (code&0x3ff)<<1
            self.assertEqual(self.v6m.R[15], pc)

    def test_c_b_backward(self):
        for code in xrange(0xe400, 0xe7ff+1):
            pc = 0x1000
            self.code_exec(pc, code)
            pc += 4
            pc += (code|~0x7ff)<<1
            pc &= 0xffffffff
            self.assertEqual(self.v6m.R[15], pc)

if __name__=="__main__":
    unittest.main()


