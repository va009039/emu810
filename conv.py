# coding:utf-8
# conv.py 2015/7/28
#
import sys
sys.path.append("C:/python27/Lib")
import os
import re
import logging
import argparse

def jumptable(lines, cpp_py):
    # def c_beq(self, code): # BEQ #0xd000-0xd0ff
    def_c = re.compile(r'def (?P<func>c_\w+).+0x(?P<start>\w{4})-0x(?P<end>\w{4})')
    def_c_2 = re.compile(r'def (?P<func>c_\w+).+0x(?P<start>\w{4})')
    indent_switch = ' '*4
    indent_case = ' '*8
    output = []
    out = indent_switch + "switch(code>>8) {"
    output.append(out)
    func_table = {}
    for s in lines:
        if def_c.search(s):
            result = def_c.search(s)
            func = result.group('func')
            start = int(result.group('start'), 16)
            end = int(result.group('end'), 16)
        elif def_c_2.search(s):
            result = def_c_2.search(s)
            func = result.group('func')
            start = int(result.group('start'), 16)
            end = start
        else:
            continue
        logging.info("%s", s)
        logging.info("func=[%s] %04x-%04x", func, start, end)
        if (start>>8) != (end>>8):
            out = indent_case + "case 0x%02x ... 0x%02x: %s(code); break;" % (start>>8, end>>8, func);
        else:
            out = indent_case + "case 0x%02x: %s(code); break;" % (start>>8, func);
        output.append(out)
        for k in xrange(start>>8, (end>>8)+1):
            assert func_table.get(k) is None, out
            func_table[k] = func
    out = indent_case + "default: c_todo(code); break;"
    output.append(out)
    out = indent_switch + "}"
    output.append(out)
    return output

def defcode(lines):
    # def e_orr(self): # ORR Rd,Rn,Rm|imm
    def_e = re.compile(r'def (?P<func>e_\w+).+# (?P<comment>.+)')
    # def c_beq(self, code): # BEQ #0xd000-0xd0ff
    def_d_c = re.compile(r'def (?P<func>(d|c)_\w+).+# (?P<comment>.+)')
    indent = 4
    output = []
    for s in lines:
        if def_e.search(s):
            result = def_e.search(s)
            out = "%svoid %s(); // %s" % (' '*indent, result.group('func'), result.group('comment'))
        elif def_d_c.search(s):
            result = def_d_c.search(s)
            out = "%svoid %s(uint32_t code); // %s" % (' '*indent, result.group('func'), result.group('comment'))
        else:
            continue
        output.append(out)
    return output

def repsub(s):
    s = re.sub(r'(0x\w{4}) <= (\w+) <= (0x\w{4})', r'\1 <= \2 && \2 <= \3', s)
    s = re.sub(r'^(\w*name) = \[(".+")\]$', r'const char* \1[] = {\2}', s)
    s = re.sub(r'^(\w+) = (.+)$', r'uint32_t \1 = \2', s)
    s = re.sub(r'^self.(d|n|m) = (immed\(.+\))$', r'R\1 = &R[\2]', s)
    s = re.sub(r'^self.(e_ldr|e_str)(\d+)\((.*)\)$', r'\1<uint\2_t>(\3)', s)
    table = [
        ("GetRegName(self.d)","GetRegName(Rd)"),
        ("GetRegName(self.n)","GetRegName(Rn)"),
        ("GetRegName(self.m)","GetRegName(Rm)"),
        ("GetRegIndex(self.n)","GetRegIndex(Rn)"),
        ("self.d +=", "Rd +="),
        ("immed=True", "true"),
        ("_ in range(k)", "int i = 0; i < k; i++"),
        ("k in [0,1,2,3,4,5,6,7]", "int k = 0; k <= 7; k++"),
        ("k in [7,6,5,4,3,2,1,0]", "int k = 7; k >= 0; k--"),
        ("self.d == _PC", "Rd == &R[_PC]"),
        ("self.d != _PC", "Rd != &R[_PC]"),
        ("self.d = _NL", "Rd = &R[_NL]"),
        ("self.n = _SP", "Rn = &R[_SP]"),
        ("self.n = _IM", "Rn = &R[_IM]"),
        ("self.m = _IM", "Rm = &R[_IM]"),
        ("self.n = self.d", "Rn = Rd"),
        ("self.R[self.d]","*Rd"), 
        ("self.R[self.n]","*Rn"), 
        ("self.R[self.m]","*Rm"),
        (" or ", " || "),
        (" and ", " && "),
        ("not (", "! ("),
        ("False", "false"),
        ("self.", ""),
    ]
    for pat in table:
        s = s.replace(pat[0], pat[1])
    return s

def code(lines):
    ignore_func = [ ]
    # def c_beq(self, code): # BEQ #0xd000-0xd0ff
    pat_indent = re.compile(r'(?P<indent>^\s*)(?P<code>.*)$')
    pat_def_e = re.compile(r'def (?P<func>e_\w+).+# (?P<comment>.+)')
    pat_def_c_d = re.compile(r'def (?P<func>(c|d)_\w+).+# (?P<comment>.+)')
    pat_elif = re.compile(r'elif (?P<cond>.+):')
    pat_if = re.compile(r'if (?P<cond>.+):')
    pat_for = re.compile(r'for (?P<cond>.+):')
    pat_else = re.compile(r'else:')
    pat_logging = re.compile(r'logging.info\((?P<msg>.+)\)$');
    pat_logging_error = re.compile(r'logging.error\((?P<msg>.+)\)$');
    pat_assert = re.compile(r'assert (?P<cond>.+)$');
    depth = -1
    depth_indent = {}
    seq = 0
    output = []
    for line in lines:
        result = pat_indent.search(line)
        assert result
        indent = result.group('indent')
        if len(indent) >= 4:
            indent = ' ' * (len(indent)-4)
        s = result.group('code')
        while depth >= 0 and indent <= depth_indent[depth]:
            out = depth_indent[depth] + "}"
            output.append(out)
            depth -= 1
        if seq == 0:
            if pat_def_c_d.search(s):
                result = pat_def_c_d.search(s)
                logging.info("%s", s)
                func = result.group('func')
                if func in ignore_func:
                    logging.info("ignore: %s", func)
                    output.append("// ignore: %s" % func)
                    output.append("")
                    continue
                out = "void BaseV6M::%s(uint32_t code) { // %s" % (func, result.group('comment'))
            elif pat_def_e.search(s):
                result = pat_def_e.search(s)
                logging.info("%s", s)
                func = result.group('func')
                if func in ignore_func:
                    logging.info("ignore: %s", func)
                    output.append("// ignore: %s" % func)
                    output.append("")
                    continue
                out = "void BaseV6M::%s() { // %s" % (func, result.group('comment'))
            else:
                continue
            depth += 1
            depth_indent[depth] = indent
            output.append(out)
            seq += 1

        elif seq == 1:
            logging.info("%s", s)
            if s == "":
                output.append("")
                seq = 0
            elif pat_elif.search(s):
                result = pat_elif.search(s)
                cond = result.group('cond')
                if output[-1] == indent + "}":
                    output[-1] = indent + "} else if (%s) {" % repsub(cond)
                else:
                    out = indent + "else if (%s) {" % repsub(cond)
                    output.append(out)
                depth += 1
                depth_indent[depth] = indent
                continue
            elif pat_if.search(s):
                result = pat_if.search(s)
                cond = result.group('cond')
                out = indent + "if (%s) {" % repsub(cond)
                output.append(out)
                depth += 1
                depth_indent[depth] = indent
                continue
            elif pat_for.search(s):
                result = pat_for.search(s)
                cond = result.group('cond')
                out = indent + "for (%s) {" % repsub(cond)
                output.append(out)
                depth += 1
                depth_indent[depth] = indent
                continue
            elif pat_else.search(s):
                if output[-1] == indent + "}":
                    output[-1] = indent + "} else {"
                else:
                    out = indent + "else {"
                    output.append(out)
                depth += 1
                depth_indent[depth] = indent
                continue
            elif pat_logging.search(s):
                result = pat_logging.search(s)
                msg = result.group('msg')
                out = indent + "V6M_INFO(%s);" % repsub(msg)
                output.append(out)
                continue
            elif pat_logging_error.search(s):
                result = pat_logging_error.search(s)
                msg = result.group('msg')
                out = indent + "V6M_ERROR(%s);" % repsub(msg)
                output.append(out)
                continue
            elif pat_assert.search(s):
                result = pat_assert.search(s)
                cond = result.group('cond')
                out = indent + "V6M_ASSERT(%s);" % repsub(cond)
                output.append(out)
            else:
                out = indent + "%s;" % repsub(s)
                output.append(out)
    return output

def bin2txt(filename, cpp_py):
    output = []
    if cpp_py:
        output.append('const char* flash_filename = "%s";' % os.path.basename(filename))
        output.append("static const uint8_t flash[] = {")
    else:
        output.append("flash = [")
    out = ""
    with open(filename, "rb") as f:
        for i,c in enumerate(f.read()):
            out += "0x%02x," % ord(c)
            if i%16 == 15:
                output.append(out)
                out = ""
    if len(out) > 0:
        output.append(out)
    if cpp_py:
        output.append("};")
    else:
        output.append("]")
    return output

if __name__=="__main__":
    #logging.basicConfig(level=logging.INFO)

    parser = argparse.ArgumentParser()
    parser.add_argument('infile', nargs=1, help=u"入力ファイル(*.py)")
    parser.add_argument('-o', '--output', help=u"出力ファイル")
    parser.add_argument('--cpp', action='store_true', default=True)
    parser.add_argument('--python', action='store_true', default=False)
    parser.add_argument('--jumptable', action='store_true')
    parser.add_argument('--defcode', action='store_true')
    parser.add_argument('--code', action='store_true')
    parser.add_argument('--bin2txt', action='store_true')

    args = parser.parse_args()
    filename = args.infile[0]
    logging.info("filename=%s", filename)

    cpp_py = args.cpp
    if args.python:
        cpp_py = False
    if args.bin2txt:
        output = bin2txt(filename, cpp_py)
    else:
        with open(filename, "r") as f:
            lines = f.read().splitlines()

    if args.jumptable:
        output = jumptable(lines, cpp_py)
    elif args.defcode:
        output = defcode(lines)
    elif args.code:
        output = code(lines)

    if args.output:
        logging.info("outputfile=%s", args.output)
        with open(args.output, "w") as f:
            for s in output:
                f.write(s + "\n")
    else:
        for s in output:
            print s


