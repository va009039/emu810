# EMU81x.py 2015/8/7
# coding:utf-8
#
import sys
import logging
import BaseV6M

EMU81x_FLASH_BASE = 0x00000000
EMU81x_RAM_BASE =   0x10000000
EMU81x_ROM_BASE =   0x1fff0000
EMU81x_APB_BASE =   0x40000000
EMU81x_AHB_BASE =   0x50000000
EMU81x_GPIO_BASE =  0xa0000000
EMU81x_ROM_SIZE = 8*1024

CMSIS_SCB_VTOR = 0xe000ed00+0x08

class EMU81x_SYSCON:
    def poke32(self, a, d):
        logging.info("P: LPC_SYSCON %08x << %08x", a, d)
    def peek32(self, a):
        d = 0x00000000
        if a == 0x4004800c or a == 0x40048044 or a == 0x40048074:
            d = 0x00000001
        logging.info("P: LPC_SYSCON %08x >> %08x", a, d)
        return d

class EMU81x_SWM:
    def __init__(self):
        self.pinassign = [0]*9

    def poke32(self, a, d):
        if (a&0xfff) <= 8*4:
            n = (a - 0x4000c000)/4
            self.pinassign[n] = d
            logging.info("P: LPC_SWM->PINASSIGN%d << %08x", n, d)
        else:
            logging.warn("P: LPC_SWM %08x << %08x", a, d)
    def peek32(self, a):
        if (a&0xfff) <= 8*4:
            n = (a - 0x4000c000)/4
            d = self.pinassign[n]
            logging.info("P: LPC_SWM->PINASSIGN%d >> %08x", n, d)
        else:
            d = 0
            logging.warn("P: LPC_SWM %08x >> %08x", a, d)
        return d

class EMU81x_USART:
    def __init__(self, mcu, ch):
        assert ch == 0 or ch == 1 or ch == 2
        self.mcu = mcu
        self.ch = ch

    def poke32(self, a, d):
        if (a&0xff) == 0x1c:
            c = chr(d&0xff)
            self.mcu.SerialPutc_Callback(self.ch, c)
            logging.info("P: LPC_USART%d->TXDATA << %08x", self.ch, d)
        else:
            logging.info("P: LPC_USART%d %08x << %08x", self.ch, a, d)

    def peek32(self, a):
        d = 0x00
        if (a&0xff) == 0x00:
            logging.info("P: LPC_USART%d->CFG >> %08x", self.ch, d)
        elif (a&0xff) == 0x08:
            d = 0x04
            if self.mcu.SerialReadable_Callback(self.ch):
                d |= 0x01
            logging.info("P: LPC_USART%d->STAT >> %08x", self.ch, d)
        elif (a&0xff) == 0x14:
            c = self.mcu.SerialGetc_Callback(self.ch)
            d = ord(c)
            logging.info("P: LPC_USART%d->RXDATA >> %08x", self.ch, d)
        elif (a&0xff) == 0x20:
            logging.info("P: LPC_USART%d->BRG >> %08x", self.ch, d)
        else:
            logging.warn("P: LPC_USART%d %08x >> %08x", self.ch, a, d)
        return d

class EMU81x_MRT:
    def __init__(self):
        self.timer0 = 0x7fffffff

    def clock_in(self, n = 1):
        assert 0x7fffffff > n >= 1
        self.timer0 = (self.timer0 - n) & 0x7fffffff

    def poke32(self, a, d):
        assert (a&0xfffff000) == 0x40004000
        if (a&0xff) == 0x00:
            logging.info("P: LPC_MRT->INTVAL0 << %02x", d)
        elif (a&0xff) == 0x08:
            logging.info("P: LPC_MRT->CTRL0 << %08x", d)
        elif (a&0xff) == 0x10:
            logging.info("P: LPC_MRT->INTVAL1 << %08x", d)
        elif (a&0xff) == 0x18:
            logging.info("P: LPC_MRT->CTRL1 << %08x", d)
        else:
            logging.warn("P: LPC_MRT %08x << %08x", a, d)

    def peek32(self, a):
        assert (a&0xfffff000) == 0x40004000
        d = 0x00000000
        if (a&0xff) == 0x04:
            d = self.timer0
            logging.info("P: LPC_MRT->TIMER0 >> %08x", d)
        else:
            logging.warn("P: LPC_MRT %08x >> %08x", a, d)
        return d

class EMU81x_IOCON:
    def __init__(self):
        self.table = {
            0x40044000: ["PIO0_17", 0x90],
            0x40044004: ["PIO0_13", 0x90],
            0x40044008: ["PIO0_12", 0x90],
            0x4004400c: ["PIO0_5",  0x90],
            0x40044010: ["PIO0_4",  0x90],
            0x40044014: ["PIO0_3",  0x90],
            0x40044018: ["PIO0_2",  0x90],
            0x4004401c: ["PIO0_11", 0x80],
            0x40044020: ["PIO0_10", 0x80],
            0x40044024: ["PIO0_16", 0x90],
            0x40044028: ["PIO0_15", 0x90],
            0x4004402c: ["PIO0_1",  0x90],
            0x40044034: ["PIO0_9",  0x90],
            0x40044038: ["PIO0_8",  0x90],
            0x4004403c: ["PIO0_7",  0x90],
            0x40044040: ["PIO0_6",  0x90],
            0x40044044: ["PIO0_0",  0x90],
            0x40044048: ["PIO0_14", 0x90],
        }

    def poke32(self, a, d):
        if a in self.table:
            self.table[a][1] = d
            logging.info("P: LPC_IOCON->%s << %08x", self.table[a][0], d) 
        else:
            logging.warn("P: LPC_IOCON %08x << %08x", a, d)

    def peek32(self, a):
        d = 0x00000000
        if a in self.table:
            d = self.table[a][1]
            logging.info("P: LPC_IOCON->%s >> %08x", self.table[a][0], d) 
        else:
            logging.warn("P: LPC_IOCON %08x >> %08x", a, d)
        return d

class EMU81x_GPIO:
    def __init__(self, mcu):
        self.mcu = mcu
        self.port = 0x00
        self.dir = 0x00
    def poke32(self, a, d):
        assert (a&0xffff0000) == 0xa0000000
        if a == 0xa0002000:
            self.dir = d
            logging.info("P: LPC_GPIO_PORT->DIR0 << %08x", d)
        elif a == 0xa0002200:
            self.port |= d
            for pin in xrange(24):
                if d & (1<<pin):
                    self.mcu.DigitalWrite_Callback(0, pin, 1)
            logging.info("P: LPC_GPIO_PORT->SET0 << %08x", d)
        elif a == 0xa0002280:
            self.port &= (~d)&0xffffffff

            for pin in xrange(24):
                if d & (1<<pin):
                     self.mcu.DigitalWrite_Callback(0, pin, 0)
            logging.info("P: LPC_GPIO_PORT->CLR0 << %08x", d)
        else:
            logging.warn("P: LPC_GPIO_PORT %08x << %08x", a, d)

    def peek32(self, a):
        assert (a&0xffff0000) == 0xa0000000
        d = 0x00000000
        if a == 0xa0002000:
            d = self.dir
            logging.info("P: LPC_GPIO_PORT->DIR0 >> %08x", d)
        elif a == 0xa0002100:
            d = self.port
            logging.info("P: LPC_GPIO_PORT->PIN0 >> %08x", d)
        else:
            logging.warn("P: LPC_GPIO_PORT %08x >> %08x", a, d)
        return d

class EMU81x_I2C:
    def __init__(self, mcu):
        self.mcu = mcu
        self.cfg = 0x00
        self.mststate = 0
        self.mstdat = 0x00
        self.i2c_addr = 0x00
        self.i2c_data = []
        self.i2c_pos = 0

    def poke32(self, a, d):
        if (a&0xff) == 0x00:
            self.cfg = d
            logging.info("P: LPC_I2C->CFG << %08x", d)
        elif (a&0xff) == 0x0c:
            logging.info("P: LPC_I2C->INTENCLR << %08x", d)
        elif (a&0xff) == 0x14:
            logging.info("P: LPC_I2C->DIV << %08x", d)
        elif (a&0xff) == 0x20:
            if d & 0x02: # master start
                self.i2c_addr = self.mstdat
                if self.i2c_addr & 0x01:
                    self.i2c_data = self.mcu.I2CRead_Callback(self.i2c_addr)
                    if self.i2c_data == None:
                        self.i2c_data = []
                    else:
                        self.mststate = 1 # receive ready
                    self.pos = 0
                else:
                    self.i2c_data = []
                    self.mststate = 2 # transmit ready
            elif d & 0x04: # master stop
                if self.i2c_addr & 0x01:
                    self.mststate = 0 # idle
                else:
                    self.mcu.I2CWrite_Callback(self.i2c_addr, self.i2c_data)
                    self.mststate = 0
            elif d & 0x01: # master continue
                self.i2c_data.append(self.mstdat)
                self.mststate = 2 # transmit ready
            else:
                assert 0, "MSTCTL=%02x" % d
            logging.info("P: LPC_I2C->MSTCTL << %08x", d)
        elif (a&0xff) == 0x24:
            logging.info("P: LPC_I2C->MSTTIME << %08x", d)
        elif (a&0xff) == 0x28:
            self.mstdat = d & 0xff
            logging.info("P: LPC_I2C->MSTDAT << %08x", d)
        else:
            logging.warn("P: LPC_I2C %08x << %08x", a, d)

    def peek32(self, a):
        d = 0x00000000
        if (a&0xff) == 0x00:
            d = self.cfg
            logging.info("P: LPC_I2C->CFG >> %08x", d)
        elif (a&0xff) == 0x04:
            d = 0x801|self.mststate<<1
            logging.info("P: LPC_I2C->STAT >> %08x", d)
        elif (a&0xff) == 0x28:
            if self.pos < len(self.i2c_data):
                d = self.i2c_data[self.pos]
                self.pos += 1
            logging.info("P: LPC_I2C->MSTDAT >> %08x", d)
        else:
            logging.warn("P: LPC_I2C %08x >> %08x", a, d)
        return d

class EMU81x_SPI:
    def __init__(self, mcu, ch):
        assert ch == 0 or ch == 1
        self.mcu = mcu
        self.ch = ch
        self.cfg = 0
        self.dly = 0
        self.rxdat = 0
        self.txdatctl = 0

    def poke32(self, a, d):
        if (a&0xff) == 0x00:
            self.cfg = d
            logging.info("P: LPC_SPI%d->CFG << %08x", self.ch, d)
        elif (a&0xff) == 0x04:
            self.dly = d
            logging.info("P: LPC_SPI%d->DLY << %08x", self.ch, d)
        elif (a&0xff) == 0x10:
            logging.info("P: LPC_SPI%d->INTENCLR << %08x", self.ch, d)
        elif (a&0xff) == 0x18:
            self.txdatctl = d
            logging.info("P: LPC_SPI%d->TXDATCTL << %08x", self.ch, d)
        elif (a&0xff) == 0x1c:
            self.rxdat = self.mcu.SPIWrite_Callback(self.ch, d)
            logging.info("P: LPC_SPI%d->TXDAT << %08x", self.ch, d)
        elif (a&0xff) == 0x24:
            logging.info("P: LPC_SPI%d->DIV << %08x", self.ch, d)
        else:
            logging.warn("P: LPC_SPI%d %08x << %08x", self.ch, a, d)

    def peek32(self, a):
        d = 0x00000000
        if (a&0xff) == 0x00:
            d = self.cfg
            logging.info("P: LPC_SPI%d->CFG >> %08x", self.ch, d)
        elif (a&0xff) == 0x04:
            d = self.dly
            logging.info("P: LPC_SPI%d->DLY >> %08x", self.ch, d)
        elif (a&0xff) == 0x08:
            d = 0x03
            logging.info("P: LPC_SPI%d->STAT >> %08x", self.ch, d)
        elif (a&0xff) == 0x14:
            d = self.rxdat
            logging.info("P: LPC_SPI%d->RXDAT >> %08x", self.ch, d)
        elif (a&0xff) == 0x18:
            d = self.txdatctl
            logging.info("P: LPC_SPI%d->TXDATCTL >> %08x", self.ch, d)
        else:
            logging.warn("P: LPC_SPI%d %08x >> %08x", self.ch, a, d)
        return d

class EMU81x_NVIC:
    def __init__(self):
        self.iser = 0x00
    def poke32(self, a, d):
        if (a&0xfff) == 0x100:
            self.iser |= d
            logging.info("P: NVIC->ISER[0] << %08x", d)
        elif (a&0xfff) == 0x180:
            logging.info("P: NVIC->ICER[0] << %08x", d)
        else:
            logging.warn("P: NVIC %08x << %08x", a, d)

class EMU81x_SCB:
    def __init__(self):
        self.vtor = 0

    def poke32(self, a, d):
        if (a&0xfff) == 0xd08:
            self.vtor = d
            logging.info("P: SCB->VTOR << %08x", d)
        else:
            logging.warn("P: SCB %08x << %08x", a, d)

    def peek32(self, a):
        d = 0
        if (a&0xfff) == 0xd08:
            d = self.vtor
            logging.info("P: SCB->VTOR >> %08x", d)
        else:
            logging.warn("P: SCB %08x >> %08x", a, d)
        return d

class EMU81x(BaseV6M.BaseV6M):
    def __init__(self, flash_size, ram_size):
        super(EMU81x, self).__init__()
        self.flash_size = flash_size
        self.ram_size = ram_size
        self.flash = [0] * self.flash_size
        self.ram = [0] * self.ram_size
        self.rom = [0] * EMU81x_ROM_SIZE
        self.syscon = EMU81x_SYSCON()
        self.swm = EMU81x_SWM()
        self.uart0 = EMU81x_USART(self, 0)
        self.uart1 = EMU81x_USART(self, 1)
        self.uart2 = EMU81x_USART(self, 2)
        self.mrt = EMU81x_MRT()
        self.iocon = EMU81x_IOCON()
        self.gpio = EMU81x_GPIO(self)
        self.i2c = EMU81x_I2C(self)
        self.spi0 = EMU81x_SPI(self, 0)
        self.spi1 = EMU81x_SPI(self, 1)
        self.scb = EMU81x_SCB()
        self.nvic = EMU81x_NVIC()
        self.SerialPutc_Callback = lambda ch,c: None
        self.SerialGetc_Callback = lambda ch: -1
        self.SerialReadable_Callback = lambda ch: 0
        self.DigitalWrite_Callback = lambda port,pin,value: value
        self.DigitalRead_Callback = lambda port,pin: 0
        self.I2CWrite_Callback = lambda addr,data: None
        self.I2CRead_Callback = lambda addr: None
        self.SPIWrite_Callback = lambda ch,value: 0

    def poke32(self, a, d):
        if a>>24 == EMU81x_RAM_BASE>>24:
            assert EMU81x_RAM_BASE <= a < EMU81x_RAM_BASE+self.ram_size
            self.ram[a - EMU81x_RAM_BASE + 0] = (d>>0) & 0xff
            self.ram[a - EMU81x_RAM_BASE + 1] = (d>>8) & 0xff
            self.ram[a - EMU81x_RAM_BASE + 2] = (d>>16) & 0xff
            self.ram[a - EMU81x_RAM_BASE + 3] = (d>>24) & 0xff
            logging.info("W: %08x %08x", a, d)
        elif a>>24 == EMU81x_APB_BASE>>24:
            if   (a>>12)&0xff == 0x04: self.mrt.poke32(a, d)
            elif (a>>12)&0xff == 0x0c: self.swm.poke32(a, d)
            elif (a>>12)&0xff == 0x44: self.iocon.poke32(a, d)
            elif (a>>12)&0xff == 0x48: self.syscon.poke32(a, d)
            elif (a>>12)&0xff == 0x50: self.i2c.poke32(a, d)
            elif (a>>12)&0xff == 0x58: self.spi0.poke32(a, d)
            elif (a>>12)&0xff == 0x5c: self.spi1.poke32(a, d)
            elif (a>>12)&0xff == 0x64: self.uart0.poke32(a, d)
            elif (a>>12)&0xff == 0x68: self.uart1.poke32(a, d)
            elif (a>>12)&0xff == 0x6c: self.uart2.poke32(a, d)
            else:
                logging.warn("P: %08x %08x", a, d)
        elif a>>24 == EMU81x_AHB_BASE>>24:
            logging.warn("P: %08x %08x", a, d)
        elif a>>24 == EMU81x_GPIO_BASE>>24:
            self.gpio.poke32(a, d)
        elif a>>24 == 0xe0:
            if 0xe000e100 <= a <= 0xe000e180:
                self.nvic.poke32(a, d)
            elif a == 0xe000ed08:
                self.scb.poke32(a, d)
            else:
                logging.warn("P: %08x %08x", a, d)
        else:
            self.trace_warn()
            assert 0, "%08x" % a

    def poke8(self, a, d):
        if a>>24 == EMU81x_RAM_BASE>>24:
            assert EMU81x_RAM_BASE <= a < EMU81x_RAM_BASE+self.ram_size
            self.ram[a - EMU81x_RAM_BASE] = d
            logging.info("W: %08x %02x", a, d)
        else:
            self.trace_warn()
            assert 0, "%08x" % a

    def peek32(self, a):
        d = 0x00000000
        if a>>24 == EMU81x_RAM_BASE>>24:
            assert EMU81x_RAM_BASE <= a < EMU81x_RAM_BASE+self.ram_size
            d = self.ram[a - EMU81x_RAM_BASE]
            d |= self.ram[a - EMU81x_RAM_BASE + 1]<<8
            d |= self.ram[a - EMU81x_RAM_BASE + 2]<<16
            d |= self.ram[a - EMU81x_RAM_BASE + 3]<<24
            logging.info("R: %08x %08x", a, d)
        elif a>>24 == EMU81x_FLASH_BASE>>24:
            assert EMU81x_FLASH_BASE <= a < EMU81x_FLASH_BASE+self.flash_size
            d = self.flash[a - EMU81x_FLASH_BASE]
            d |= self.flash[a - EMU81x_FLASH_BASE + 1]<<8
            d |= self.flash[a - EMU81x_FLASH_BASE + 2]<<16
            d |= self.flash[a - EMU81x_FLASH_BASE + 3]<<24
            logging.info("R: %08x %08x", a, d)
        elif a>>24 == EMU81x_ROM_BASE>>24:
            assert EMU81x_ROM_BASE <= a < EMU81x_ROM_BASE+EMU81x_ROM_SIZE
            d = self.rom[a - EMU81x_ROM_BASE]
            d |= self.rom[a - EMU81x_ROM_BASE + 1]<<8
            d |= self.rom[a - EMU81x_ROM_BASE + 2]<<16
            d |= self.rom[a - EMU81x_ROM_BASE + 3]<<24
            logging.info("R: %08x %08x", a, d)
        elif a>>24 == EMU81x_APB_BASE>>24:
            if   (a>>12)&0xff == 0x04: d = self.mrt.peek32(a)
            elif (a>>12)&0xff == 0x0c: d = self.swm.peek32(a)
            elif (a>>12)&0xff == 0x44: d = self.iocon.peek32(a)
            elif (a>>12)&0xff == 0x48: d = self.syscon.peek32(a)
            elif (a>>12)&0xff == 0x50: d = self.i2c.peek32(a)
            elif (a>>12)&0xff == 0x58: d = self.spi0.peek32(a)
            elif (a>>12)&0xff == 0x5c: d = self.spi1.peek32(a)
            elif (a>>12)&0xff == 0x64: d = self.uart0.peek32(a)
            elif (a>>12)&0xff == 0x68: d = self.uart1.peek32(a)
            elif (a>>12)&0xff == 0x6c: d = self.uart2.peek32(a)
            else:
                logging.warn("P: %08x", a)
        elif a>>24 == EMU81x_GPIO_BASE>>24:
            d = self.gpio.peek32(a)
        elif a>>24 == 0xe0:
            if  a == 0xe000ed08:
                d = self.scb.peek32(a)
            else:
                logging.warn("P: %08x", a)
        else:
            assert 0, "%08x" % a
        assert 0x00 <= d <= 0xffffffff
        return d

    def peek8(self, a):
        d = 0x00
        if a>>24 == EMU81x_RAM_BASE>>24:
            assert EMU81x_RAM_BASE <= a < EMU81x_RAM_BASE+self.ram_size
            d = self.ram[a - EMU81x_RAM_BASE]
            logging.info("R: %08x %02x", a, d)
        elif a>>24 == EMU81x_FLASH_BASE>>24:
            assert EMU81x_FLASH_BASE <= a < EMU81x_FLASH_BASE+self.flash_size
            d = self.flash[a - EMU81x_FLASH_BASE]
            logging.info("R: %08x %02x", a, d)
        elif a>>24 == EMU81x_ROM_BASE>>24:
            assert EMU81x_ROM_BASE <= a < EMU81x_ROM_BASE+EMU81x_ROM_SIZE
            d = self.rom[a - EMU81x_ROM_BASE]
            logging.info("R: %08x %02x", a, d)
        else:
            assert 0, "%08x" % a
        assert 0x00 <= d <= 0xff
        return d

    def clock_in(self, n):
        self.mrt.clock_in(n)

    def trace(self):
        logging.info("S: r0=%08x r1=%08x  r2=%08x  r3=%08x  r4=%08x r5=%08x r6=%08x r7=%08x", self.R[0], self.R[1], self.R[2], self.R[3], self.R[4], self.R[5], self.R[6], self.R[7])
        logging.info("S: r8=%08x r9=%08x r10=%08x r11=%08x r12=%08x sp=%08x lr=%08x pc=%08x", self.R[8], self.R[9], self.R[10], self.R[11], self.R[12], self.R[13], self.R[14], self.R[15])
        logging.info("S: xPSR=%08x N=%d Z=%d C=%d V=%d", self.R[16], self.N(), self.Z(), self.C(), self.V())
        logging.debug("S: cycle=%d code=%02x code2nd=%02x im=%08x d=%d n=%d m=%d", self.cycle, self.code, self.code2nd, self.R[17], self.d, self.n, self.m)

    def trace_warn(self):
        logging.warn("S: r0=%08x r1=%08x  r2=%08x  r3=%08x  r4=%08x r5=%08x r6=%08x r7=%08x", self.R[0], self.R[1], self.R[2], self.R[3], self.R[4], self.R[5], self.R[6], self.R[7])
        logging.warn("S: r8=%08x r9=%08x r10=%08x r11=%08x r12=%08x sp=%08x lr=%08x pc=%08x", self.R[8], self.R[9], self.R[10], self.R[11], self.R[12], self.R[13], self.R[14], self.R[15])
        logging.warn("S: xPSR=%08x N=%d Z=%d C=%d V=%d", self.R[16], self.N(), self.Z(), self.C(), self.V())
        logging.warn("S: cycle=%d code=%02x code2nd=%02x im=%08x d=%d n=%d m=%d", self.cycle, self.code, self.code2nd, self.R[17], self.d, self.n, self.m)

    def load_flash(self, filename):
        with open(filename, "rb") as f:
            for i,c in enumerate(f.read()):
                self.flash[i] = ord(c)

    def load_rom(self, filename):
        with open(filename, "rb") as f:
            for i,c in enumerate(f.read()):
                self.rom[i] = ord(c)

