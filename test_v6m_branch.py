# test_v6m_branch.py 2015/8/24
# coding:utf-8
#
import random
import logging
import unittest
import BaseV6M

_SP = 13
_LR = 14
_PC = 15


def SignExtend32(value, width):
    if value & (1<<(width-1)):
        return value - (1<<width)
    return value

def Bits(value, width, base):
    return (value>>base) & ((1<<width)-1)

def signed_immed(code, width, base = 0): # signed immed
    i = immed(code, width, base)
    if i & (1<<(width-1)):
        return i - (1<<width)
    return i
    
def mask32(addr):
    return addr & 0xffffffff

class MyV6M(BaseV6M.BaseV6M):
    def __init__(self):
        super(MyV6M, self).__init__()
        self.mem = {}
    def poke32(self, a, d):
        self.poke8(a, d & 0xff)
        self.poke8(a+1, (d>>8)&0xff)
        self.poke8(a+2, (d>>16)&0xff)
        self.poke8(a+3, (d>>24)&0xff)
    def poke8(self, a, d):
        self.mem[a] = d
    def peek32(self, a):
        return self.peek8(a)|self.peek8(a+1)<<8|self.peek8(a+2)<<16|self.peek8(a+3)<<24
    def peek8(self, a):
        if a in self.mem:
            return self.mem[a]
        return 0x00
    def trace(self, a):
        pass

class Test_BaseV6M(unittest.TestCase):
    def setUp(self):
        self.v6m = MyV6M()
        self.v6m.reset()

    def tearDown(self):
        pass

    def code_exec(self, pc, code, code2nd=0):
        if code:
            self.v6m.mem[pc] = code&0xff
            self.v6m.mem[pc+1] = code>>8
            self.v6m.mem[pc+2] = code2nd&0xff
            self.v6m.mem[pc+3] = code2nd>>8
        self.v6m.jump(pc)
        self.v6m.run(1)

    def test_c32_mrs(self):
        for code in [0xf3ef]:
            for code2nd in xrange(0x8000, 0x8fff+1):
                pc = 0x1000
                self.code_exec(pc, code, code2nd)
                self.assertEqual(self.v6m.R[_PC], pc+4)

    def test_c32_msr(self):
        for code in xrange(0xf380, 0xf38f+1):
            for code2nd in xrange(0x8800, 0x88ff+1):
                pc = 0x1000
                self.code_exec(pc, code, code2nd)
                self.assertEqual(self.v6m.R[_PC], pc+4)

    def test_c32_dsb_dmb_isb(self):
        for code in [0xf3bf]:
            for code2nd in [0x8f4f, 0x8f5f, 0x8f6f]:
                pc = 0x1000
                self.code_exec(pc, code, code2nd)
                self.assertEqual(self.v6m.R[_PC], pc+4)

    def test_c_bl(self):
        #code_range = [0xf000, 0xf100, 0xf200, 0xf2ff, 0xf400, 0xf4ff, 0xf5ff, 0xf6ff, 0xf7ff]
        code_range = xrange(0xf000, 0xf7ff+1)
        for code in code_range:
            code2nd_range = [0xf800, 0xf8ff, 0xff00, 0xffff]
            code2nd_range = [0xf800, 0xf8ff, 0xff00, 0xffff, 0xd000, 0xdfff, 0xf000, 0xf7ff]
            for code2nd in code2nd_range:
                assert (code&0xf800) == 0xf000 and (code2nd&0xd000) == 0xd000
                pc = 0x1000
                old_pc = pc
                self.code_exec(pc, code, code2nd)
                self.assertEqual(self.v6m.R[_LR], old_pc+2+3)
                s = Bits(code,1,10)
                imm10 = Bits(code,10,0)
                j1 = Bits(code2nd,1,13)
                j2 = Bits(code2nd,1,11)
                imm11 = Bits(code2nd,11, 0)
                i1 = 1 - (j1 ^ s)
                i2 = 1 - (j2 ^ s)
                imm32 = SignExtend32(s<<24|i1<<23|i2<<22|imm10<<12|imm11<<1, 25)
                #print "code=0x%04x:0x%04x addr=0x%08x" % (code, code2nd, addr)
                self.assertEqual(self.v6m.R[_PC], mask32(old_pc+2+2+imm32))

    def test_c_bl1(self):
        #code_range = [0xf000, 0xf100, 0xf200, 0xf2ff, 0xf400, 0xf4ff, 0xf5ff, 0xf6ff, 0xf7ff]
        code_range = xrange(0xf000, 0xf7ff+1)
        for code in code_range:
            code2nd_range = [0xf800, 0xf8ff, 0xff00, 0xffff]
            for code2nd in code2nd_range:
                pc = 0x1000
                old_pc = pc
                self.code_exec(pc, code, code2nd)
                self.assertEqual(self.v6m.R[_LR], old_pc+2+3)
                addr = (code&0x7ff)<<12|(code2nd&0x07ff)<<1
                if addr & 0x00400000: # backward
                    addr |= 0xff800000
                #print "code=0x%04x:0x%04x addr=0x%08x" % (code, code2nd, addr)
                self.assertEqual(self.v6m.R[_PC], mask32(old_pc+2+2+addr))

    def test_c_bl2(self):
        #code_range = [0xf000, 0xf100, 0xf200, 0xf2ff, 0xf400, 0xf4ff, 0xf5ff, 0xf6ff, 0xf7ff]
        code_range = xrange(0xf000, 0xf7ff+1)
        for code in code_range:
            code2nd_range = [0xf800, 0xf8ff, 0xff00, 0xffff]
            for code2nd in code2nd_range:
                pc = 0x1000
                old_pc = pc
                self.code_exec(pc, code, code2nd)
                self.assertEqual(self.v6m.R[_LR], old_pc+2+3)
                addr = BaseV6M.signed_immed(code,11)<<12|BaseV6M.immed(code2nd,11)<<1
                #print "code=0x%04x:0x%04x addr=0x%08x" % (code, code2nd, addr)
                self.assertEqual(self.v6m.R[_PC], mask32(old_pc+2+2+addr))

    def test_c_bl3(self):
        #code_range = [0xf000, 0xf100, 0xf200, 0xf2ff, 0xf400, 0xf4ff, 0xf5ff, 0xf6ff, 0xf7ff]
        code_range = xrange(0xf000, 0xf7ff+1)
        for code in code_range:
            code2nd_range = [0xf800, 0xf8ff, 0xff00, 0xffff]
            for code2nd in code2nd_range:
                pc = 0x1000
                old_pc = pc
                self.code_exec(pc, code, code2nd)
                self.assertEqual(self.v6m.R[_LR], old_pc+2+3)
                addr = BaseV6M.signed_immed((code&0x7ff)<<12|(code2nd&0x7ff)<<1, 23)
                #print "code=0x%04x:0x%04x addr=0x%08x" % (code, code2nd, addr)
                self.assertEqual(self.v6m.R[_PC], mask32(old_pc+2+2+addr))


    def test_c_b(self):
        for code in xrange(0xe000, 0xe7ff+1):
            pc = 0x1000
            self.code_exec(pc, code)
            pc += 4
            pc += BaseV6M.signed_immed(code, 11) * 2
            self.assertEqual(self.v6m.R[15], pc)

    def test_c_b_forward(self):
        for code in xrange(0xe000, 0xe3ff+1):
            pc = 0x1000
            self.code_exec(pc, code)
            pc += 4
            pc += (code&0x3ff)<<1
            self.assertEqual(self.v6m.R[15], pc)

    def test_c_b_backward(self):
        for code in xrange(0xe400, 0xe7ff+1):
            pc = 0x1000
            self.code_exec(pc, code)
            pc += 4
            pc += (code|~0x7ff)<<1
            pc &= 0xffffffff
            self.assertEqual(self.v6m.R[15], pc)

if __name__=="__main__":
    unittest.main()


