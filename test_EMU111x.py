# test_EMU111x.py 2015/8/7
# coding:utf-8
#
import sys
import logging
import unittest
import EMU111x

SUCCESS_KEY = "success"
FAIL_KEY = "fail"

class Test_EMU111x(unittest.TestCase):
    def setUp(self):
        self.mcu = EMU111x.EMU111x(flash_size=32*1024, ram_size=4*1024)

        self.mcu.SerialPutc_Callback = self.SerialPutc_Callback
        self.mcu.DigitalWrite_Callback = self.DigitalWrite_Callback
        self.mcu.I2CWrite_Callback = self.I2CWrite_Callback
        self.mcu.I2CRead_Callback = self.I2CRead_Callback
        self.mcu.SPIWrite_Callback = self.SPIWrite_Callback
        self.output = ""
        self.success = False
        self.fail = False
        self.PCA9672_addr = (0x23<<1)
        self.PCA9672_data = 0x00

    def tearDown(self):
        pass

    def SerialPutc_Callback(self, ch, c):
        sys.stdout.write(c)
        self.output += c
        if self.output.find(SUCCESS_KEY) >= 0:
            self.success = True
        elif self.output.find(FAIL_KEY) >= 0:
            self.fail = True

    def DigitalWrite_Callback(self, port, pin, value):
        print "PIO%d_%d=%d" % (port, pin, value)

    def DigitalRead_Callback(self, port, pin):
        return 0

    def I2CWrite_Callback(self, addr, data):
        print "*** I2CWrite(addr=%02x, %s)" % (addr, map(lambda x:"0x%02x" % x, data))
        return 0

    def I2CRead_Callback(self, addr):
        if (addr&0xfe) == self.PCA9672_addr:
            data = [self.PCA9672_data]
            self.PCA9672_data ^= 0x80
        else:
            data = [0x00]
        print "*** I2CRead(addr=%02x, %s)" % (addr, map(lambda x:"0x%02x" % x, data))
        return data

    def SPIWrite_Callback(self, ch, value):
        data = value + 1;
        print "*** SPIWrite(ch=%d, wr:%02x) rd:%02x" % (ch, value, data)
        return data

    def test_spi(self):
        filename = r"test_spi_LPC1114.bin"
        self.mcu.load_flash(filename)
        self.mcu.reset()
        clk = 0
        for _ in xrange(50000):
            self.mcu.fetch()
            self.mcu.execute()
            clk += 1
            if clk > 500:
                self.mcu.clock_in(40000)
                clk = 0
            if self.success or self.fail:
                break
        self.assertTrue(self.success)

    def test_i2c(self):
        filename = r"test_i2c_LPC1114.bin"
        self.mcu.load_flash(filename)
        self.mcu.reset()
        ck = 0
        for _ in xrange(50000):
            self.mcu.fetch()
            self.mcu.execute()
            ck += 1
            if ck > 500:
                self.mcu.clock_in(40000)
                ck = 0
            if self.success or self.fail:
                break
        self.assertTrue(self.success)

    def test_blinky(self):
        filename = r"test_blinky_LPC1114.bin"
        logging.info("filename=%s", filename)
        self.mcu.load_flash(filename)
        self.mcu.reset()
        ck = 0
        for _ in xrange(90000):
            self.mcu.fetch()
            self.mcu.execute()
            ck += 1
            if ck > 500:
                self.mcu.clock_in(40000)
                ck = 0
            if self.success or self.fail:
                break
        self.assertTrue(self.success)

    def test_serial(self):
        filename = r"test_serial_LPC1114.bin"
        logging.info("filename=%s", filename)
        self.mcu.load_flash(filename)
        self.mcu.reset()
        ck = 0
        for _ in xrange(200000):
            self.mcu.fetch()
            self.mcu.execute()
            ck += 1
            if ck > 500:
                self.mcu.clock_in(40000)
                ck = 0
            if self.success or self.fail:
                break
        self.assertTrue(self.success)

if __name__=="__main__":
    #logging.basicConfig(level=logging.INFO)
    #logging.basicConfig(level=logging.INFO, filename="R:/test_BaseV6M.log")
    unittest.main()


