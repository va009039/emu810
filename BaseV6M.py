# BaseV6M.py 2015/8/25
# coding: utf-8
import abc
import logging
import sys

_SP = 13
_LR = 14
_PC = 15
_xPSR = 16
_IM = 17
_NL = 18

NZCVlookup = { # (d,m,n): (N,Z,C,V)
            (0,0,0): 0<<31|0<<30|0<<29|0<<28,
            (1,0,1): 1<<31|0<<30|0<<29|0<<28,
            (1,0,0): 1<<31|0<<30|0<<29|1<<28,
            (0,0,1): 0<<31|0<<30|1<<29|0<<28,
            (1,1,0): 1<<31|0<<30|0<<29|0<<28,
            (0,1,1): 0<<31|0<<30|1<<29|1<<28,
            (0,1,0): 0<<31|0<<30|1<<29|0<<28,
            (1,1,1): 1<<31|0<<30|1<<29|0<<28,}

def GetRegName(i):
    name = ["r0","r1","r2","r3","r4","r5","r6","r7","r8","r9","r10","r11","r12","sp","lr","pc", "_xPSR","_IM","_NL"]
    return name[i]

def GetSysName(i):
    if i == 0x08: return "MSP"
    if i == 0x09: return "PSP"
    if i == 0x10: return "PRIMASK"
    if i == 0x14: return "CONTROL"
    return "*undef*"
    
def immed(code, width, base = 0): # unsigned immed
    return (code>>base) & ((1<<width)-1)

def signed_immed(code, width, base = 0): # signed immed
    i = immed(code, width, base)
    if i & (1<<(width-1)):
        return i - (1<<width)
    return i

def add32(a, b):
    return (a + b) & 0xffffffff

def mul32(a, b):
    return (a * b) & 0xffffffff

def not32(data):
    return (~data)&0xffffffff

def align32(addr):
    return addr & 0xfffffffc

def align16(addr):
    return addr & 0xfffffffe

class BaseV6M(object):
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        self.R = [0]*19 # r0,r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15, xPSR, immed-data, null
        self.n = 0 # Rn
        self.d = 0 # Rd
        self.m = 0 # Rm
        self.op = 0
        self.reg_list = 0
        self.cycle = 0
        self.cache = False
        self.table = [
            self.c_lsl, self.c_lsl, self.c_lsl, self.c_lsl, self.c_lsl, self.c_lsl, self.c_lsl, self.c_lsl, # 00-07
            self.c_lsr, self.c_lsr, self.c_lsr, self.c_lsr, self.c_lsr, self.c_lsr, self.c_lsr, self.c_lsr, # 08-0f
            self.c_asr, self.c_asr, self.c_asr, self.c_asr, self.c_asr, self.c_asr, self.c_asr, self.c_asr, # 10-17
            self.c_add, self.c_add, self.c_sub, self.c_sub, self.c_add1, self.c_add1, self.c_sub1, self.c_sub1, # 18-1f
            self.c_mov, self.c_mov, self.c_mov, self.c_mov, self.c_mov, self.c_mov, self.c_mov, self.c_mov, # 20-27
            self.c_cmp, self.c_cmp, self.c_cmp, self.c_cmp, self.c_cmp, self.c_cmp, self.c_cmp, self.c_cmp, # 28-2f
            self.c_add2, self.c_add2, self.c_add2, self.c_add2, self.c_add2, self.c_add2, self.c_add2, self.c_add2, # 30-37
            self.c_sub2, self.c_sub2, self.c_sub2, self.c_sub2, self.c_sub2, self.c_sub2, self.c_sub2, self.c_sub2, # 38-3f
            self.c_and_eor_lsl_lsr, self.c_asr_adc_sbc_ror, self.c_tst_neg_cmp_cmn, self.c_orr_mul_bic_mvn, self.c_add_hr, self.c_cmp_hr, self.c_mov_hr, self.c_bx, # 40-47
            self.c_ldr_pc, self.c_ldr_pc, self.c_ldr_pc, self.c_ldr_pc, self.c_ldr_pc, self.c_ldr_pc, self.c_ldr_pc, self.c_ldr_pc, # 48-4f
            self.c_str2, self.c_str2, self.c_strh2, self.c_strh2, self.c_strb2, self.c_strb2, self.c_ldrsb, self.c_ldrsb, # 50-57
            self.c_ldr2, self.c_ldr2, self.c_ldrh2, self.c_ldrh2, self.c_ldrb2, self.c_ldrb2, self.c_ldrsh, self.c_ldrsh, # 58-5f
            self.c_str1, self.c_str1, self.c_str1, self.c_str1, self.c_str1, self.c_str1, self.c_str1, self.c_str1, # 60-67
            self.c_ldr1, self.c_ldr1, self.c_ldr1, self.c_ldr1, self.c_ldr1, self.c_ldr1, self.c_ldr1, self.c_ldr1, # 68-6f
            self.c_strb, self.c_strb, self.c_strb, self.c_strb, self.c_strb, self.c_strb, self.c_strb, self.c_strb, # 70-77
            self.c_ldrb, self.c_ldrb, self.c_ldrb, self.c_ldrb, self.c_ldrb, self.c_ldrb, self.c_ldrb, self.c_ldrb, # 78-7f
            self.c_strh, self.c_strh, self.c_strh, self.c_strh, self.c_strh, self.c_strh, self.c_strh, self.c_strh, # 80-87
            self.c_ldrh, self.c_ldrh, self.c_ldrh, self.c_ldrh, self.c_ldrh, self.c_ldrh, self.c_ldrh, self.c_ldrh, # 88-8f
            self.c_str_sp, self.c_str_sp, self.c_str_sp, self.c_str_sp, self.c_str_sp, self.c_str_sp, self.c_str_sp, self.c_str_sp, # 90-97
            self.c_ldr_sp, self.c_ldr_sp, self.c_ldr_sp, self.c_ldr_sp, self.c_ldr_sp, self.c_ldr_sp, self.c_ldr_sp, self.c_ldr_sp, # 98-6f
            self.c_add_r_pc, self.c_add_r_pc, self.c_add_r_pc, self.c_add_r_pc, self.c_add_r_pc, self.c_add_r_pc, self.c_add_r_pc, self.c_add_r_pc, # a0-a7
            self.c_add_r_sp, self.c_add_r_sp, self.c_add_r_sp, self.c_add_r_sp, self.c_add_r_sp, self.c_add_r_sp, self.c_add_r_sp, self.c_add_r_sp, # a8-af
            self.c_add_sp, self.c_todo, self.c_uxt, self.c_todo, self.c_push, self.c_push_lr, self.c_cps, self.c_todo, # b0-b7
            self.c_todo, self.c_todo, self.c_rev, self.c_todo, self.c_pop, self.c_pop_pc, self.c_bkpt, self.c_nop, # b8-bf
            self.c_stm, self.c_stm, self.c_stm, self.c_stm, self.c_stm, self.c_stm, self.c_stm, self.c_stm, # c0-c7
            self.c_ldm, self.c_ldm, self.c_ldm, self.c_ldm, self.c_ldm, self.c_ldm, self.c_ldm, self.c_ldm, # c8-cf
            self.c_beq, self.c_bne, self.c_bcs, self.c_bcc, self.c_bmi, self.c_bpl, self.c_bvs, self.c_bvc, # d0-d7
            self.c_bhi, self.c_bls, self.c_bge, self.c_blt, self.c_bgt, self.c_ble, self.c_todo, self.c_swi, # d8-df
            self.c_b_forward, self.c_b_forward, self.c_b_forward, self.c_b_forward, self.c_b_backward, self.c_b_backward, self.c_b_backward, self.c_b_backward, # e0-e7
            self.c_todo, self.c_todo, self.c_todo, self.c_todo, self.c_todo, self.c_todo, self.c_todo, self.c_todo, # e8-ef
            self.c32_bl_forward, self.c32_bl_forward, self.c32_bl_forward, self.c32_bl_forward3, self.c32_bl_backward, self.c32_bl_backward, self.c32_bl_backward, self.c32_bl_backward, # f0-f7
            self.c_todo, self.c_todo, self.c_todo, self.c_todo, self.c_todo, self.c_todo, self.c_todo, self.c_todo, # f8-ff
        ]
    @abc.abstractmethod
    def trace(self):
        pass

    @abc.abstractmethod
    def poke8(self, a, d):
        pass

    @abc.abstractmethod
    def peek8(self, a):
        pass

    def poke16(self, a, w):
        self.poke8(a, w & 0xff)
        self.poke8(a+1, w>>8)

    def peek16(self, a):
        return self.peek8(a) | self.peek8(a+1)<<8

    @abc.abstractmethod
    def poke32(self, a, d):
        pass

    @abc.abstractmethod
    def peek32(self, a):
        pass

    def GetRegIndex(self, pR):
        return pR

    def StrRegLists(self):
        s = []
        for k in [0,1,2,3,4,5,6,7]:
            if self.reg_list & (1<<k):
                s.append(GetRegName(k))
        return ",".join(s)

    def N(self):
        return immed(self.R[_xPSR], 1, 31)
    def Z(self):
        return immed(self.R[_xPSR], 1, 30)
    def C(self):
        return immed(self.R[_xPSR], 1, 29)
    def V(self):
        return immed(self.R[_xPSR], 1, 28)
    def Cin(self, c):
        assert c == 0 or c == 1
        self.R[_xPSR] &= ~(1<<29)
        self.R[_xPSR] |= c<<29

    def NZupdate(self, d):
        assert (d>>32) == 0
        if d & (1<<31): # N
            mask = 1<<31|0<<30
        elif d == 0: # Z
            mask = 0<<31|1<<30
        else:
            mask = 0<<31|0<<30
        self.R[_xPSR] &= ~(1<<31|1<<30)
        self.R[_xPSR] |= mask

    def NZCVupdate(self, d, n, m):
        assert (d>>32) == 0
        mask = NZCVlookup[(d>>31,n>>31,m>>31)]
        if d == 0:
            mask |= 1<<30 # Z=1
        self.R[_xPSR] &= ~(1<<31|1<<30|1<<29|1<<28)
        self.R[_xPSR] |= mask

    def jump(self, addr):
        self.R[_PC] = align16(addr)
        self.cache = False

    def exception_entry(self, num):
        self.push(self.R[_xPSR])
        self.push(self.R[_PC] - 2)
        self.push(self.R[_LR])
        self.push(self.R[13])
        self.push(self.R[3])
        self.push(self.R[2])
        self.push(self.R[1])
        self.push(self.R[0])
        self.R[_LR] = 0xfffffff9
        addr = self.peek32(num * 4)
        assert addr&1
        self.jump(addr)
        logging.info("I: Exception entry %d", num)

    def exception_return(self):
        if (self.R[_PC]&0xfffffff0) == 0xfffffff0:
            assert (self.R[_PC]&0x0e) == 8
            self.R[0] = self.pop()
            self.R[1] = self.pop()
            self.R[2] = self.pop()
            self.R[3] = self.pop()
            self.R[12] = self.pop()
            self.R[_LR] = self.pop()
            addr = self.pop()
            self.R[_xPSR] = self.pop()
            self.jump(addr)
            logging.info("I: Exception return ; =0x%08x", addr)

    def push(self, d):
        self.R[_SP] -= 4
        self.poke32(self.R[_SP], d)

    def pop(self):
        d = self.peek32(self.R[_SP])
        self.R[_SP] += 4
        return d

    def d_Rn_Rd(self, code): # decode leaf
        self.d = immed(code, 3, 0)
        self.n = immed(code, 3, 3)

    def d_Op_H_Rm(self, code): # BX Rm / BLX Rm
        self.m = immed(code, 4, 3)
        self.op = immed(code, 1, 7)

    def d_imm3_Rn_Rd(self, code): # ADD Rd,Rn,#imm3
        self.d_Rn_Rd(code)
        self.R[_IM] = immed(code, 3, 6)
        self.m = _IM

    def d_Rd_imm8(self, code): # <Op> Rd,#imm8 / LDR rd,[pc,#imm8]
        self.d = immed(code, 3, 8)
        self.n = self.d
        self.R[_IM] = immed(code, 8, 0)
        self.m = _IM

    def d_Rn_imm8(self, code): # <Op> Rn,#imm8
        self.n = immed(code, 3, 8)
        self.R[_IM] = immed(code, 8, 0)
        self.m = _IM

    def d_imm5_Rn_Rd(self, code): # LSL Rd,Rn,#imm5 / LDR Rd,[Rn,#imm5]
        self.d_Rn_Rd(code)
        self.R[_IM] = immed(code, 5, 6)
        self.m = _IM

    def d_Op_Rm_Rd(self, code): # # <Op> Rd,Rm
        self.d = immed(code, 3, 0)
        self.n = self.d
        self.m = immed(code, 3, 3)
        self.op = immed(code, 4, 6)

    def d_Op_imm7(self, code): # ADD|SUB sp,sp,#imm7
        self.R[_IM] = immed(code, 7, 0)
        self.m = _IM
        self.op = immed(code, 1, 7)

    def d_D_M_Rm_Rd(self, code): # ADD|CMP|MOV Rd,Rm
        self.m = immed(code, 4, 3)
        self.d = immed(code, 3, 0)
        if code & 0x80:
            self.d += 8
        self.n = self.d

    def d_Rm_Rn_Rd(self, code): # LDR Rd,[Rn,Rm]
        self.d_Rn_Rd(code)
        self.m = immed(code, 3, 6)

    def d_reg_list(self, code): # PUSH / POP
        self.reg_list = immed(code, 8, 0)

    def d_Rn_reg_list(self, code): # LDM / STM
        self.d_reg_list(code)
        self.n = immed(code, 3, 8)

    def d32_Rd_SYSm(self, code, code2nd): # MRS
        self.d = immed(code2nd, 4, 8)
        self.m = immed(code2nd, 7, 0)

    def d32_Rn_SYSm(self, code, code2nd): # MSR
        self.n = immed(code, 4, 0)
        self.m = immed(code2nd, 7, 0)

    def e_adc(self): # ADC Rd,Rn,Rm|imm
        n = self.R[self.n]
        m = self.R[self.m]
        d = add32(n, m + self.C())
        self.R[self.d] = d
        self.NZCVupdate(d, n, m)

    def e_sbc(self): # SBC Rd,Rn,Rm|imm
        n = self.R[self.n]
        m = not32(self.R[self.m])
        d = add32(n, m + self.C())
        self.R[self.d] = d
        self.NZCVupdate(d, n, m)

    def e_add(self): # ADD Rd,Rn,Rm|imm
        n = self.R[self.n]
        m = self.R[self.m]
        d = add32(n, m)
        self.R[self.d] = d
        self.NZCVupdate(d, n, m)

    def e_sub(self): # SUB Rd,Rn,Rm|imm
        n = self.R[self.n]
        m = not32(self.R[self.m])
        d = add32(n, m + 1)
        self.R[self.d] = d
        self.NZCVupdate(d, n, m)

    def e_cmp(self): # CMP Rn,Rm|imm
        self.d = _NL
        self.e_sub()

    def e_cmn(self): # CMN Rn,Rm|imm
        self.d = _NL
        self.e_add()

    def e_and(self): # AND Rd,Rn,Rm|imm
        data = self.R[self.n] & self.R[self.m]
        self.R[self.d] = data
        self.NZupdate(data)

    def e_tst(self): # TST Rn,Rm|imm
        self.d = _NL
        self.e_and()

    def e_lsl(self): # LSL Rd,Rn,Rm|imm
        data = self.R[self.n]
        shift = self.R[self.m] & 0xff
        if shift <= 32:
            self.Cin((data>>(32-shift))&1)
            data <<= shift
            data &= 0xffffffff
        else:
            self.Cin(0)
            data = 0
        self.R[self.d] = data
        self.NZupdate(data)

    def e_lsr(self): # LSR Rd,Rn,Rm|imm
        data = self.R[self.n]
        shift = self.R[self.m] & 0xff
        if shift >= 1:
            if shift <= 32:
                self.Cin((data>>(shift-1))&1)
                data >>= shift
            else:
                self.Cin(0)
                data = 0
        self.R[self.d] = data
        self.NZupdate(data)

    def e_asr(self): # ASR Rd,Rn,Rm|imm
        data = self.R[self.n]
        if (data & 0x80000000) == 0:
            self.e_lsr()
            return
        shift = self.R[self.m] & 0xff
        if shift >= 1:
            if shift <= 31:
                self.Cin((data>>(shift-1))&1)
                data >>= shift
                data |= ~(0x7fffffff>>(shift-1))
                data &= 0xffffffff
            else:
                self.Cin(1)
                data = 0xffffffff
        self.R[self.d] = data
        self.NZupdate(data)

    def e_ror(self): # ROR Rd,Rn,Rm|imm
        data = self.R[self.n]
        shift = self.R[self.m] & 0xff
        if shift >= 1:
            k = shift % 32
            if k == 0:
                self.Cin(data>>31)
            else:
                for _ in range(k):
                    if data & 1:
                        self.Cin(1)
                        data >>= 1
                        data |= 0x80000000
                    else:
                        self.Cin(0)
                        data >>= 1
        self.R[self.d] = data
        self.NZupdate(data)

    def e_orr(self): # ORR Rd,Rn,Rm|imm
        data = self.R[self.n] | self.R[self.m]
        self.R[self.d] = data
        self.NZupdate(data)

    def e_eor(self): # EOR Rd,Rn,Rm|imm
        data = self.R[self.n] ^ self.R[self.m]
        self.R[self.d] = data
        self.NZupdate(data)

    def e_mul(self): # MUL Rd,Rn,Rm
        data = mul32(self.R[self.n], self.R[self.m])
        self.R[self.d] = data
        self.NZupdate(data)

    def e_bic(self): # BIC Rd,Rn,Rm|imm
        data = self.R[self.n] & not32(self.R[self.m])
        self.R[self.d] = data
        self.NZupdate(data)

    def e_mov(self): # MOV Rd,Rm|imm
        data = self.R[self.m]
        self.R[self.d] = data
        self.NZupdate(data)

    def e_neg(self): # NEG
        self.n = _IM
        self.R[_IM] = 0
        self.e_sub()

    def e_mvn(self): # MVN
        data = not32(self.R[self.m])
        self.R[self.d] = data
        self.NZupdate(data)

# Branch Operation

    def c_beq(self, code): # BEQ 0xd000-0xd0ff
        addr = self.R[_PC] + signed_immed(code, 8) * 2
        if self.Z() == 1:
            self.jump(addr)
        logging.info("I: BEQ 0x%08x", addr)

    def c_bne(self, code): # BNE 0xd100-0xd1ff
        addr = self.R[_PC] + signed_immed(code, 8) * 2
        if self.Z() == 0:
            self.jump(addr)
        logging.info("I: BNE 0x%08x", addr)

    def c_bcs(self, code): # BCS 0xd200-0xd2ff
        addr = self.R[_PC] + signed_immed(code, 8) * 2
        if self.C() == 1:
            self.jump(addr)
        logging.info("I: BCS 0x%08x", addr)

    def c_bcc(self, code): # BCC 0xd300-0xd3ff
        addr = self.R[_PC] + signed_immed(code, 8) * 2
        if self.C() == 0:
            self.jump(addr)
        logging.info("I: BCC 0x%08x", addr)

    def c_bmi(self, code): # BMI 0xd400-0xd4ff
        addr = self.R[_PC] + signed_immed(code, 8) * 2
        if self.N() == 1:
            self.jump(addr)
        logging.info("I: BMI 0x%08x", addr)

    def c_bpl(self, code): # BMI 0xd500-0xd5ff
        addr = self.R[_PC] + signed_immed(code, 8) * 2
        if self.N() == 0:
            self.jump(addr)
        logging.info("I: BPL 0x%08x", addr)

    def c_bvs(self, code): # BVS 0xd600-0xd6ff
        addr = self.R[_PC] + signed_immed(code, 8) * 2
        if self.V() == 1:
            self.jump(addr)
        logging.info("I: BVS 0x%08x", addr)
        assert 0

    def c_bvc(self, code): # BVC 0xd700-0xd7ff
        addr = self.R[_PC] + signed_immed(code, 8) * 2
        if self.V() == 0:
            self.jump(addr)
        logging.info("I: BVC 0x%08x", addr)
        assert 0

    def c_bhi(self, code): # BHI 0xd800-0xd8ff
        addr = self.R[_PC] + signed_immed(code, 8) * 2
        if self.C() == 1 and self.Z() == 0:
            self.jump(addr)
        logging.info("I: BHI 0x%08x", addr)

    def c_bls(self, code): # BLS 0xd900-0xd9ff
        addr = self.R[_PC] + signed_immed(code, 8) * 2
        if self.C() == 0 or self.Z() == 1:
            self.jump(addr)
        logging.info("I: BLS 0x%08x", addr)

    def c_bge(self, code): # BGE 0xda00-0xdaff
        addr = self.R[_PC] + signed_immed(code, 8) * 2
        if self.N() == self.V():
            self.jump(addr)
        logging.info("I: BGE 0x%08x", addr)

    def c_blt(self, code): # BLT 0xdb00-0xdbff
        addr = self.R[_PC] + signed_immed(code, 8) * 2
        if self.N() != self.V():
            self.jump(addr)
        logging.info("I: BLT 0x%08x", addr)

    def c_bgt(self, code): # BGT 0xdc00-0xdcff
        addr = self.R[_PC] + signed_immed(code, 8) * 2
        if self.Z() == 0 and self.N() == self.V():
            self.jump(addr)
        logging.info("I: BGT 0x%08x", addr)

    def c_ble(self, code): # BLE 0xdd00-0xddff
        addr = self.R[_PC] + signed_immed(code, 8) * 2
        if self.Z() == 1 or self.N() != self.V():
            self.jump(addr)
        logging.info("I: BLE 0x%08x", addr)

    def c_b_forward(self, code): # B 0xe000-0xe3ff
        addr = self.R[_PC] + ((code&0x3ff)<<1)
        self.jump(addr)
        logging.info("I: B 0x%08x ; forward", addr)

    def c_b_backward(self, code): # B 0xe400-0xe7ff
        addr = self.R[_PC] + ((code|~0x7ff)<<1)
        addr &= 0xffffffff
        self.jump(addr)
        logging.info("I: B 0x%08x ; backward", addr)

    def c32_bl_forward(self, code): # BL 0xf000-0xf2ff
        self.R[_LR] = self.R[_PC] | 1
        imm32 = immed(code, 10)<<12|immed(self.code2nd, 11)<<1
        if (self.code2nd & (1<<13)) == 0: # J1
            imm32 |= (1<<23)
        if (self.code2nd & (1<<11)) == 0: # J2
            imm32 |= (1<<22)
        addr = self.R[_PC] + imm32
        addr &= 0xffffffff
        assert (self.code2nd&0xd000) == 0xd000
        self.jump(addr)
        logging.info("I: BL 0x%08x ; forward", addr)

    def c32_bl_forward3(self, code): # BL 0xf300-0xf3ff
        if (self.code2nd&0xd000) == 0xd000:
            self.c32_bl_forward(code)
            return
        if (code&0xf0) == 0x80:
            self.c32_msr(code, self.code2nd)
        elif (code&0xff) == 0xbf:
            self.c32_dsb_dmb_isb(code, self.code2nd)
        elif (code&0xff) == 0xef:
            self.c32_mrs(code, self.code2nd)
        else:
            assert 0

    def c32_mrs(self, code, code2nd):
        self.d32_Rd_SYSm(code, code2nd)
        self.jump(self.R[_PC])
        logging.info("I: MRS %s,%s", GetRegName(self.d), GetSysName(self.m))

    def c32_msr(self, code, code2nd):
        self.d32_Rn_SYSm(code, code2nd)
        self.jump(self.R[_PC])
        logging.info("I: MSR %s,%s", GetSysName(self.m), GetRegName(self.n))

    def c32_dsb_dmb_isb(self, code, code2nd):
        if code2nd == 0x8f4f:
            self.jump(self.R[_PC])
            logging.warn("I: DSB.W")
        elif code2nd == 0x8f5f:
            self.jump(self.R[_PC])
            logging.warn("I: DMB.W")
        elif code2nd == 0x8f6f:
            self.jump(self.R[_PC])            
            logging.warn("I: ISB.W")
        else:
            assert 0

    def c32_bl_backward(self, code): # BL 0xf400-0xf7ff
        self.R[_LR] = self.R[_PC] | 1
        imm32 = 0xff000000|immed(code, 10)<<12|immed(self.code2nd, 11)<<1
        if (self.code2nd & (1<<13)): # J1
            imm32 |= (1<<23)
        if (self.code2nd & (1<<11)): # J2
            imm32 |= (1<<22)
        addr = self.R[_PC] + imm32
        assert (self.code2nd&0xd000) == 0xd000
        addr &= 0xffffffff
        self.jump(addr)
        logging.info("I: BL 0x%08x ; backward", addr)

    def c_bx(self, code): # BX BLX 0x4770
        self.d_Op_H_Rm(code)
        if self.op == 1: # BLX ?
            self.R[_LR] = (self.R[_PC] - 2) | 1
        addr = self.R[self.m] & 0xfffffffe
        self.jump(addr)
        name = ["BX", "BLX"]
        logging.info("I: %s %s ; =0x%x", name[self.op], GetRegName(self.m), addr)
        self.exception_return()

# ALU Operation

    def c_add(self, code): # ADDS Rd,Rn,Rm 0x1800-0x19ff
        self.d_Rm_Rn_Rd(code)
        self.e_add()
        logging.info("I: ADDS %s,%s,%s", GetRegName(self.d), GetRegName(self.n), GetRegName(self.m))

    def c_sub(self, code): # SUBS Rd,Rn,Rm 0x1a00-0x1bff
        self.d_Rm_Rn_Rd(code)
        self.e_sub()
        logging.info("I: SUBS %s,%s,%s", GetRegName(self.d), GetRegName(self.n), GetRegName(self.m))

    def c_add1(self, code): # ADD Rd,Rn,#imm3 / MOV Rd,Rn 0x1c00-0x1dff
        self.d_imm3_Rn_Rd(code)
        self.e_add()
        if (code&0x01c0) == 0x0000: # imm3=#0x00
            logging.info("I: MOV %s,%s", GetRegName(self.d), GetRegName(self.n))
            assert (code&0xffc0) == 0x1c00
        else:
            logging.info("I: ADD %s,%s,#0x%02x", GetRegName(self.d), GetRegName(self.n), self.R[_IM])

    def c_sub1(self, code): # SUB Rd,Rn,#imm3 0x1e00-0x1fff
        self.d_imm3_Rn_Rd(code)
        assert self.R[_IM] != 0
        self.e_sub()
        logging.info("I: SUB %s,%s,#0x%02x", GetRegName(self.d), GetRegName(self.n), self.R[_IM])

    def c_add2(self, code): # ADD Rd,#imm8 0x3000-0x37ff
        self.d_Rd_imm8(code)
        self.e_add()
        logging.info("I: ADD %s,#0x%02x", GetRegName(self.d), self.R[_IM])

    def c_sub2(self, code): # SUB Rd,#imm8 0x3800-0x3fff
        self.d_Rd_imm8(code)
        self.e_sub()
        logging.info("I: SUB %s,#0x%02x", GetRegName(self.d), self.R[_IM])

    def c_mov(self, code): # MOVS Rd,#imm8 0x2000-0x27ff
        self.d_Rd_imm8(code)
        self.e_mov()
        logging.info("I: MOVS %s,#0x%02x", GetRegName(self.d), self.R[_IM])

    def c_cmp(self, code): # CMP Rn,#imm8 0x2800-0x2fff
        self.d_Rn_imm8(code)
        self.e_cmp()
        logging.info("I: CMP %s,#0x%02x", GetRegName(self.n), self.R[_IM])

    def c_lsl(self, code): # LSLS Rd,Rn,#imm5 0x0000-0x07ff
        self.d_imm5_Rn_Rd(code)
        self.e_lsl()
        logging.info("I: LSLS %s,%s,#%d", GetRegName(self.d), GetRegName(self.n), self.R[_IM])

    def c_lsr(self, code): # LSRS Rd,Rn,#imm5 0x0800-0x0fff
        self.d_imm5_Rn_Rd(code)
        if self.R[_IM] == 0:
            self.R[_IM] = 32
        self.e_lsr()
        logging.info("I: LSRS %s,%s,#%d", GetRegName(self.d), GetRegName(self.n), self.R[_IM])

    def c_asr(self, code): # ASRS Rd,Rn,#imm5 0x1000-0x17ff
        self.d_imm5_Rn_Rd(code)
        if self.R[_IM] == 0:
            self.R[_IM] = 32
        self.e_asr()
        logging.info("I: ASRS %s,%s,#%d", GetRegName(self.d), GetRegName(self.n), self.R[_IM])

    def c_and_eor_lsl_lsr(self, code): # ANDS|EORS|LSLS|LSRS Rd,Rm 0x4000-0x40ff
        self.d_Op_Rm_Rd(code)
        if self.op == 0:
            self.e_and()
            logging.info("I: ANDS %s,%s,%s", GetRegName(self.d), GetRegName(self.n), GetRegName(self.m))
        elif self.op == 1:
            self.e_eor()
            logging.info("I: EORS %s,%s,%s", GetRegName(self.d), GetRegName(self.n), GetRegName(self.m))
        elif self.op == 2:
            self.e_lsl()
            logging.info("I: LSLS %s,%s,%s", GetRegName(self.d), GetRegName(self.n), GetRegName(self.m))
        else:
            self.e_lsr()
            logging.info("I: LSRS %s,%s,%s", GetRegName(self.d), GetRegName(self.n), GetRegName(self.m))

    def c_asr_adc_sbc_ror(self, code): # ASRS|ADCS|SBCS|RORS Rd,Rm 0x4100-0x41ff
        self.d_Op_Rm_Rd(code)
        if self.op == 4:
            self.e_asr()
            logging.info("I: ASRS %s,%s,%s", GetRegName(self.d), GetRegName(self.n), GetRegName(self.m))
        elif self.op == 5:
            self.e_adc()
            logging.info("I: ADCS %s,%s,%s", GetRegName(self.d), GetRegName(self.n), GetRegName(self.m))
        elif self.op == 6:
            self.e_sbc()
            logging.info("I: SBCS %s,%s,%s", GetRegName(self.d), GetRegName(self.n), GetRegName(self.m))
        else:
            self.e_ror()
            logging.info("I: RORS %s,%s,%s", GetRegName(self.d), GetRegName(self.n), GetRegName(self.m))

    def c_tst_neg_cmp_cmn(self, code): # TST|NEGS|CMP|CMN Rd,Rm 0x4200-0x42ff
        self.d_Op_Rm_Rd(code)
        if self.op == 8:
            self.e_tst()
            logging.info("I: TST %s,%s", GetRegName(self.n), GetRegName(self.m))
        elif self.op == 9:
            self.e_neg()
            logging.info("I: NEG %s,%s", GetRegName(self.n), GetRegName(self.m))
        elif self.op == 10:
            self.e_cmp()
            logging.info("I: CMP %s,%s", GetRegName(self.n), GetRegName(self.m))
        else:
            self.e_cmn()
            logging.info("I: CMN %s,%s", GetRegName(self.n), GetRegName(self.m))

    def c_orr_mul_bic_mvn(self, code): # ORRS|MULS|BICS|MVNS Rd,Rm 0x4300-0x43ff
        self.d_Op_Rm_Rd(code)
        if self.op == 12:
            self.e_orr()
            logging.info("I: ORRS %s,%s,%s", GetRegName(self.d), GetRegName(self.n), GetRegName(self.m))
        elif self.op == 13:
            self.e_mul()
            logging.info("I: MULS %s,%s,%s", GetRegName(self.d), GetRegName(self.n), GetRegName(self.m))
        elif self.op == 14:
            self.e_bic()
            logging.info("I: BICS %s,%s,%s", GetRegName(self.d), GetRegName(self.n), GetRegName(self.m))
        else:
            self.e_mvn()
            logging.info("I: MVNS %s,%s,%s", GetRegName(self.d), GetRegName(self.n), GetRegName(self.m))

    def c_add_hr(self, code): # ADD Rd,Rm (high reg) 0x4400-0x44ff
        self.d_D_M_Rm_Rd(code)
        self.R[self.d] = add32(self.R[self.n], self.R[self.m])
        if self.d == _PC:
            self.jump(self.R[_PC])
            logging.info("I: ADD pc,pc,%s ; pc=0x%08x", GetRegName(self.m), self.R[_PC])
            self.exception_return()
        else:
            logging.info("I: ADD %s,%s,%s", GetRegName(self.d), GetRegName(self.n), GetRegName(self.m))

    def c_cmp_hr(self, code): # CMP Rd,Rm (high reg) 0x4500-0x45ff
        self.d_D_M_Rm_Rd(code)
        assert self.d != _PC
        assert self.m != _PC
        self.d = _NL
        self.e_cmp()
        logging.info("I: CMP %s,%s", GetRegName(self.n), GetRegName(self.m))

    def c_mov_hr(self, code): # MOV Rd,Rm (high reg) 0x4600-0x46ff
        self.d_D_M_Rm_Rd(code)
        self.R[self.d] = self.R[self.m]
        if self.d == _PC:
            self.jump(self.R[_PC])
            logging.info("I: MOV pc,%s", GetRegName(self.m))
            self.exception_return()
        else:
            logging.info("I: MOV %s,%s", GetRegName(self.d), GetRegName(self.m))

    def c_add_r_pc(self, code): # ADD Rd,pc,#imm8 0xa000-0xa7ff
        self.d_Rd_imm8(code)
        off = self.R[_IM] * 4
        addr = align32(self.R[_PC]) + off
        self.R[self.d] = addr
        logging.info("I: ADD %s,pc,#0x%02x ; @0x%08x", GetRegName(self.d), off, addr)

    def c_add_r_sp(self, code): # ADD Rd,sp,#imm8 0xa800-0xafff
        self.d_Rd_imm8(code)
        off = self.R[_IM] * 4
        addr = self.R[self.d] = self.R[_SP] + off
        self.R[self.d] = addr
        logging.info("I: ADD %s,sp,#0x%02x ; @0x%08x", GetRegName(self.d), off, addr)
        assert addr%4 == 0

    def c_add_sp(self, code): # ADD|SUB sp,sp,#imm 0xb000-0xb0ff
        self.d_Op_imm7(code)
        data = self.R[_IM] * 4
        if self.op == 0: # ADD
            self.R[_SP] += data
        else: # SUB
            self.R[_SP] -= data
        name = ["ADD", "SUB"]
        logging.info("I: %s sp,sp,#0x%x", name[self.op], data)

# Load Store Operation

    def e_ldr32(self, immed = False):
        if immed:
            self.R[_IM] *= 4
        addr = self.R[self.n] + self.R[self.m]
        data = self.peek32(addr)
        self.R[self.d] = data

    def e_str32(self, immed = False):
        if immed:
            self.R[_IM] *= 4
        addr = self.R[self.n] + self.R[self.m]
        data = self.R[self.d]
        self.poke32(addr, data)

    def e_ldr16(self, immed = False):
        if immed:
            self.R[_IM] *= 2
        addr = self.R[self.n] + self.R[self.m]
        data = self.peek16(addr)
        self.R[self.d] = data

    def e_str16(self, immed = False):
        if immed:
            self.R[_IM] *= 2
        addr = self.R[self.n] + self.R[self.m]
        data = self.R[self.d]
        self.poke16(addr, data)

    def e_ldr8(self, immed = False):
        addr = self.R[self.n] + self.R[self.m]
        data = self.peek8(addr)
        self.R[self.d] = data

    def e_str8(self, immed = False):
        addr = self.R[self.n] + self.R[self.m]
        data = self.R[self.d]
        self.poke8(addr, data)

    def c_ldr1(self, code): # LDR Rd,[Rn,#imm5] 0x6800-0x6fff
        self.d_imm5_Rn_Rd(code)
        self.e_ldr32(immed=True)
        logging.info("I: LDR %s,[%s,#0x%02x]", GetRegName(self.d), GetRegName(self.n), self.R[_IM])

    def c_str1(self, code): # STR Rd,[Rn,#imm5] 0x6000-0x67ff
        self.d_imm5_Rn_Rd(code)
        self.e_str32(immed=True)
        logging.info("I: STR %s,[%s,#0x%02x]", GetRegName(self.d), GetRegName(self.n), self.R[_IM])

    def c_ldrb(self, code): # LDRB Rd,[Rn,#imm5] 0x7800-0x7fff
        self.d_imm5_Rn_Rd(code)
        self.e_ldr8(immed=True)
        logging.info("I: LDRB %s,[%s,#0x%02x]", GetRegName(self.d), GetRegName(self.n), self.R[_IM])

    def c_strb(self, code): # STRB Rd,[Rn,#imm5] 0x7000-0x77ff
        self.d_imm5_Rn_Rd(code)
        self.e_str8(immed=True)
        logging.info("I: STRB %s,[%s,#0x%02x]", GetRegName(self.d), GetRegName(self.n), self.R[_IM])

    def c_ldrh(self, code): # LDRH Rd,[Rn,#imm5] 0x8800-0x8fff
        self.d_imm5_Rn_Rd(code)
        self.e_ldr16(immed=True)
        logging.info("I: LDRH %s,[%s,#0x%02x]", GetRegName(self.d), GetRegName(self.n), self.R[_IM])
 
    def c_strh(self, code): # STRH Rd,[Rn,#imm5] 0x8000-0x87ff
        self.d_imm5_Rn_Rd(code)
        self.e_str16(immed=True)
        logging.info("I: STRH %s,[%s,#0x%02x]", GetRegName(self.d), GetRegName(self.n), self.R[_IM])
 
    def c_ldr2(self, code): # LDR Rd,[Rn,Rm] 0x5800-0x59ff
        self.d_Rm_Rn_Rd(code)
        self.e_ldr32()
        logging.info("I: LDR %s,[%s,%s]", GetRegName(self.d), GetRegName(self.n), GetRegName(self.m))

    def c_str2(self, code): # STR Rd,[Rn,Rm] 0x5000-0x51ff
        self.d_Rm_Rn_Rd(code)
        self.e_str32()
        logging.info("I: STR %s,[%s,%s]", GetRegName(self.d), GetRegName(self.n), GetRegName(self.m))
        self.trace()

    def c_ldrh2(self, code): # LDRH Rd,[Rn,Rm] 0x5a00-0x5bff
        self.d_Rm_Rn_Rd(code)
        self.e_ldr16()
        logging.info("I: LDRH %s,[%s,%s]", GetRegName(self.d), GetRegName(self.n), GetRegName(self.m))

    def c_strh2(self, code): # STRH Rd,[Rn,Rm] 0x5200-0x53ff
        self.d_Rm_Rn_Rd(code)
        self.e_str16()
        logging.info("I: STRH %s,[%s,%s]", GetRegName(self.d), GetRegName(self.n), GetRegName(self.m))

    def c_ldrsh(self, code): # LDRSH Rd,[Rn,Rm] 0x5e00-0x5fff
        self.d_Rm_Rn_Rd(code)
        self.e_ldr16()
        data = signed_immed(self.R[self.d], 16) & 0xffffffff
        self.R[self.d] = data
        logging.info("I: LDRSH %s,[%s,%s] ; =0x%x", GetRegName(self.d), GetRegName(self.n), GetRegName(self.m), data)

    def c_ldrb2(self, code): # LDRB Rd,[Rn,Rm] 0x5c00-0x5dff
        self.d_Rm_Rn_Rd(code)
        self.e_ldr8()
        logging.info("I: LDRB %s,[%s,%s]", GetRegName(self.d), GetRegName(self.n), GetRegName(self.m))

    def c_strb2(self, code): # STRB Rd,[Rn,Rm] 0x5400-0x55ff
        self.d_Rm_Rn_Rd(code)
        self.e_str8()
        logging.info("I: STRB %s,[%s,%s]", GetRegName(self.d), GetRegName(self.n), GetRegName(self.m))

    def c_ldrsb(self, code): # LDRSB Rd,[Rn,Rm] 0x5600-0x57ff
        self.d_Rm_Rn_Rd(code)
        self.e_ldr8()
        data = signed_immed(self.R[self.d], 8) & 0xffffffff
        self.R[self.d] = data
        logging.info("I: LDRSB %s,[%s,%s] ; =0x%x", GetRegName(self.d), GetRegName(self.n), GetRegName(self.m), data)

    def c_ldr_pc(self, code): # LDR Rd,[pc,#imm8] 0x4800-0x4fff
        self.d_Rd_imm8(code)
        off = self.R[self.m] * 4
        addr = align32(self.R[_PC]) + off
        data = self.peek32(addr)
        self.R[self.d] = data
        logging.info("I: LDR %s,[pc,#%d] ; =0x%x @0x%08x", GetRegName(self.d), off, data, addr)

    def c_ldr_sp(self, code): # LDR Rd,[sp,#imm8] 0x9800-0x9fff
        self.d_Rd_imm8(code)
        self.n = _SP
        self.e_ldr32(immed=True)
        logging.info("I: LDR %s,[sp,#0x%x]", GetRegName(self.d), self.R[_IM])

    def c_str_sp(self, code): # STR Rd,[sp,#imm8] 0x9000-0x97ff
        self.d_Rd_imm8(code)
        self.n = _SP
        self.e_str32(immed=True)
        logging.info("I: STR %s,[sp,#0x%x]", GetRegName(self.d), self.R[_IM])

# Push Pop Operation

    def e_ldm(self): # LDM / POP
        addr = self.R[self.n]
        for k in [0,1,2,3,4,5,6,7]:
            if self.reg_list & (1<<k):
                self.R[k] = self.peek32(addr)
                addr += 4
        if not (self.reg_list & (1<<self.GetRegIndex(self.n))):
            self.R[self.n] = addr

    def e_stm(self): # STM
        addr = self.R[self.n]
        for k in [0,1,2,3,4,5,6,7]:
            if self.reg_list & (1<<k):
                self.poke32(addr, self.R[k])
                addr += 4
        self.R[self.n] = addr

    def e_push(self): # PUSH
        for k in [7,6,5,4,3,2,1,0]:
            if self.reg_list & (1<<k):
                self.push(self.R[k])

    def c_ldm(self, code): # LDM Rn!,{reg_list} 0xc800-0xcfff
        self.d_Rn_reg_list(code)
        self.e_ldm()
        logging.info("I: LDMIA %s!,{%s}", GetRegName(self.n), self.StrRegLists())

    def c_stm(self, code): # STM Rn!,{reg_list} 0xc000-0xc7ff
        self.d_Rn_reg_list(code)
        self.e_stm()
        logging.info("I: STMIA %s!,{%s}", GetRegName(self.n), self.StrRegLists())

    def c_pop(self, code): # POP {reg_list} 0xbc00-0xbcff
        self.d_reg_list(code)
        self.n = _SP
        self.e_ldm()
        logging.info("I: POP {%s}", self.StrRegLists())

    def c_pop_pc(self, code): # POP {reg_list,pc} 0xbd00-0xbdff
        self.d_reg_list(code)
        self.n = _SP
        self.e_ldm()
        self.jump(self.pop())
        logging.info("I: POP {%s,pc}", self.StrRegLists())
        self.exception_return()

    def c_push(self, code): # PUSH {reg_list} 0xb400-0xb4ff
        self.d_reg_list(code)
        self.e_push()
        logging.info("I: PUSH {%s}", self.StrRegLists())

    def c_push_lr(self, code): # PUSH {reg_list,lr} 0xb500-0xb5ff
        self.d_reg_list(code)
        self.push(self.R[_LR])
        self.e_push()
        logging.info("I: PUSH {%s,lr}", self.StrRegLists())

# Misc Operation

    def c_uxt(self, code): # UXTH|UXTB Rd,Rm 0xb200-0xb2ff
        self.d_Op_Rm_Rd(code)
        if self.op == 10:
            self.R[self.d] = self.R[self.m] & 0xffff
            logging.info("I: UXTH %s,%s", GetRegName(self.d), GetRegName(self.m))
            assert (code&0xffc0) == 0xb280
        elif self.op == 11:
            self.R[self.d] = self.R[self.m] & 0xff
            logging.info("I: UXTB %s,%s", GetRegName(self.d), GetRegName(self.m))
            assert (code&0xffc0) == 0xb2c0
        elif self.op == 8:
            self.R[self.d] = signed_immed(self.R[self.m], 16) & 0xffffffff
            logging.info("I: SXTH %s,%s", GetRegName(self.d), GetRegName(self.m))
            assert (code&0xffc0) == 0xb200
        elif self.op == 9:
            self.R[self.d] = signed_immed(self.R[self.m], 8) & 0xffffffff
            logging.info("I: SXTB %s,%s", GetRegName(self.d), GetRegName(self.m))
            assert (code&0xffc0) == 0xb240
        else:
            assert 0

    def c_rev(self, code): # REV|REV16|REVSH Rd,Rm 0xba00-0xba3f
        self.d_Op_Rm_Rd(code)
        if self.op == 8:
            data = self.R[self.m]
            self.R[self.d] = ((data<<24)&0xff000000)|((data<<8)&0xff0000)|((data>>8)&0xff00)|data>>24
            logging.info("I: REV %s,%s", GetRegName(self.d), GetRegName(self.m))
        if self.op == 9:
            data = self.R[self.m]
            self.R[self.d] = ((data<<8)&0xff00ff00)|((data>>8)&0x00ff00ff)
            logging.info("I: REV16 %s,%s", GetRegName(self.d), GetRegName(self.m))
        if self.op == 11: # REVSH
            data = self.R[self.m]
            self.R[self.d] = ((data<<8)&0xff00)|((data>>8)&0x00ff)
            if self.R[self.d] & 0x8000:
                self.R[self.d] |= 0xffff0000
            logging.info("I: REVSH %s,%s", GetRegName(self.d), GetRegName(self.m))
        else:
            assert 0

    def c_nop(self, code): # NOP 0xbf00
        assert code == 0xbf00
        logging.info("I: NOP")

    def c_bkpt(self, code): # BKPT #imm8 0xbe00-0xbeff
        assert 0

    def c_swi(self, code): # SWI #imm8 0xdf00-0xdfff
        logging.info("I: SWI #%d", immed(code, 8, 0))
        self.exception_entry(11)

    def c_cps(self, code): # CPSIE|CPSID A|I|F 0xb6c0-0xb6e7
        i = immed(code, 1,4)
        f = immed(code, 3,0)
        assert f != 0
        i_name = ["CPSIE", "CPSID"]
        f_name = ["", "F", "I", "IF", "A", "AF", "AI", "AIF"]
        logging.info("I: %s %s", i_name[i], f_name[f])

    def c_todo(self, code): # undefined code
        self.trace()
        logging.error("I: ASSERT!!! %04x %04x", code, self.code2nd)
        assert 0

    def fetch(self):
        assert (self.R[_PC]&1) == 0
        if self.cache:
            self.code = self.code2nd
            self.code2nd = self.peek16(self.R[_PC])
            self.R[_PC] += 2
        else:
            d = self.peek32(self.R[_PC])
            self.code = d & 0xffff
            self.code2nd = d>>16
            self.R[_PC] += 4
            self.cache = True

    def execute(self):
        self.table[self.code>>8](self.code)
        self.cycle += 1

    def reset(self):
        self.R[_SP] = self.peek32(0x00000000)
        self.R[_PC] = self.peek32(0x00000004)
        logging.info("reset sp=0x%08x pc=0x%08x",self.R[_SP], self.R[_PC])
        self.jump(self.R[_PC])

    def run(self, step):
        for _ in xrange(step):
            self.fetch()
            self.execute()

if __name__=="__main__":
    logging.basicConfig(level=logging.INFO)
    class myv6m(BaseV6M):
        def __init__(self):
            super(ips, self).__init__()

